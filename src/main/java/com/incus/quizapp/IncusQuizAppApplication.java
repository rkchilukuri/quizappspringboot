package com.incus.quizapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class IncusQuizAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(IncusQuizAppApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedOrigins("http://localhost:4200","http://ec2-13-233-19-209.ap-south-1.compute.amazonaws.com",
						"http://ec2-13-233-19-209.ap-south-1.compute.amazonaws.com:8080") .allowedMethods("*")
	            .allowedHeaders("*");;
			}

			@Override
			public void addResourceHandlers(ResourceHandlerRegistry registry) {

				registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");

				registry.addResourceHandler("/webjars/**")
						.addResourceLocations("classpath:/META-INF/resources/webjars/");

			}
		};
	}
}
