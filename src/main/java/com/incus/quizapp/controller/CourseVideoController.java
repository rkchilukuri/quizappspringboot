/**
 * 
 */
package com.incus.quizapp.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incus.quizapp.model.TitleInput;
import com.incus.quizapp.model.IdInput;
import com.incus.quizapp.model.CourseVideo;
import com.incus.quizapp.model.CourseVideoLst;
import com.incus.quizapp.model.TournamentInput;
import com.incus.quizapp.service.CourseVideoService;
import com.incus.quizapp.util.Constants;

import io.swagger.annotations.ApiOperation;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 21-Dec-2020
 * Description:
 * *******************************************************
 */
@RestController
@RequestMapping("/courseVideo")
public class CourseVideoController {
	@Autowired
	CourseVideoService courseVideoService;
	private static final Logger logger = LoggerFactory.getLogger(CourseVideoController.class);
	
	@ApiOperation(value="Add CourseVideo")
	@PostMapping(value="/add")
	public ResponseEntity<?> addCourseVideo(@RequestBody CourseVideo courseVideo){
		logger.debug("In Add CourseVideo");
		CourseVideoLst courseVideoLst = new CourseVideoLst();
		courseVideoLst.setState(false);
		if(courseVideo.getTitle()!=null && !courseVideo.getTitle().trim().isEmpty()) {
			courseVideo.setCreatedDate(LocalDateTime.now());
			courseVideo.setId(null);
			courseVideo.setModifiedDate(null);
			courseVideo.setModifiedBy(null);
			CourseVideo courseVideoAdded = courseVideoService.addCourseVideo(courseVideo);
			List<CourseVideo> lstCourseVideo = new ArrayList();
			lstCourseVideo.add(courseVideoAdded);
			courseVideoLst.setCourseVideoList(lstCourseVideo);
			courseVideoLst.setState(true);
			courseVideoLst.setMessage("SUCCESS");
			if(courseVideoAdded!=null) {
				return new ResponseEntity<>(courseVideoLst,HttpStatus.OK);
			}else {
				courseVideoLst.setMessage(Constants.ERR_CRS_001);
				return new ResponseEntity<>(courseVideoLst,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			courseVideoLst.setMessage(Constants.ERR_CRS_004);
			return new ResponseEntity<>(courseVideoLst,HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value="Update CourseVideo")
	@PostMapping(value="/update")
	public ResponseEntity<?> updateCourseVideo(@RequestBody CourseVideo courseVideo){
		logger.debug("In Update CourseVideo");
		CourseVideoLst courseVideoLst = new CourseVideoLst();
		courseVideoLst.setState(false);
		if(courseVideo.getTitle()!=null && !courseVideo.getTitle().trim().isEmpty()) {
			courseVideo.setModifiedDate(LocalDateTime.now());
			CourseVideo courseVideoUpdated = courseVideoService.updateCourseVideo(courseVideo);
			List<CourseVideo> lstCourseVideo = new ArrayList();
			lstCourseVideo.add(courseVideoUpdated);
			courseVideoLst.setCourseVideoList(lstCourseVideo);
			courseVideoLst.setState(true);
			courseVideoLst.setMessage("SUCCESS");
			if(courseVideoUpdated!=null) {
				return new ResponseEntity<>(courseVideoLst,HttpStatus.OK);
			}else {
				courseVideoLst.setMessage(Constants.ERR_CRS_001);
				return new ResponseEntity<>(courseVideoLst,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			courseVideoLst.setMessage(Constants.ERR_CRS_004);
			return new ResponseEntity<>(courseVideoLst,HttpStatus.BAD_REQUEST);
		}
	}
	@ApiOperation(value="Get CourseVideo")
	@PostMapping(value="/getCourseVideo")
	public ResponseEntity<?> getCourseVideo(@RequestBody IdInput courseVideoId){
		logger.debug("In Get CourseVideo By Id");
		CourseVideoLst courseVideoLst = new CourseVideoLst();
		courseVideoLst.setState(false);
		CourseVideo extCourseVideo = courseVideoService.getCourseVideoById(courseVideoId.getId());
		if(extCourseVideo!=null) {
			List<CourseVideo> lstCourseVideo = new ArrayList();
			lstCourseVideo.add(extCourseVideo);
			courseVideoLst.setCourseVideoList(lstCourseVideo);
			courseVideoLst.setState(true);
			courseVideoLst.setMessage("SUCCESS");
			return new ResponseEntity<>(courseVideoLst,HttpStatus.OK);
		}else {
			courseVideoLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(courseVideoLst,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get CourseVideo By Chapter")
	@PostMapping(value="/getCourseVideoByChapter")
	public ResponseEntity<?> getCourseVideoByChapter(@RequestBody TitleInput chapterName){
		logger.debug("In Get CourseVideo by Name");
		CourseVideoLst courseVideoLst = new CourseVideoLst();
		courseVideoLst.setState(false);
		courseVideoLst.setMessage("FAIL");
		List<CourseVideo> extCourseVideoLst = courseVideoService.getCourseVideoByChapter(chapterName.getTitle());
		if(extCourseVideoLst!=null && extCourseVideoLst.size()>0) {
			courseVideoLst.setCourseVideoList(extCourseVideoLst);
			courseVideoLst.setState(true);
			courseVideoLst.setMessage("SUCCESS");
			return new ResponseEntity<>(extCourseVideoLst,HttpStatus.OK);
		}else {
			courseVideoLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(Constants.ERR_CRS_002,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get CourseVideo By ExamCategory Course and Chapter")
	@PostMapping(value="/getCourseVideoByExamCategoryCourseChapter")
	public ResponseEntity<?> getCourseVideoByExamCategoryCourseChapter(@RequestBody TournamentInput chapterName){
		logger.debug("In Get CourseVideo by Name");
		CourseVideoLst courseVideoLst = new CourseVideoLst();
		courseVideoLst.setState(false);
		courseVideoLst.setMessage("FAIL");
		List<CourseVideo> extCourseVideoLst = courseVideoService.getCourseVideoByExamCategoryCourseChapter(chapterName.getExamCategory(),chapterName.getCourse(),chapterName.getChapter());
		if(extCourseVideoLst!=null && extCourseVideoLst.size()>0) {
			courseVideoLst.setCourseVideoList(extCourseVideoLst);
			courseVideoLst.setState(true);
			courseVideoLst.setMessage("SUCCESS");
			return new ResponseEntity<>(courseVideoLst,HttpStatus.OK);
		}else {
			courseVideoLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(courseVideoLst,HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation(value="Get All CourseVideos")
	@PostMapping(value="/getAll")
	public ResponseEntity<?> getAll(){
		logger.debug("In Get All CourseVideos");
		List<CourseVideo> lstCourseVideo = courseVideoService.getAllCourseVideos();
		CourseVideoLst courseVideoLst = new CourseVideoLst();
		courseVideoLst.setCourseVideoList(lstCourseVideo);
		courseVideoLst.setState(true);
		courseVideoLst.setMessage("SUCCESS");
 		if(lstCourseVideo!=null) {
			return new ResponseEntity<>(courseVideoLst,HttpStatus.OK);
		}else {
			courseVideoLst.setState(false);
			courseVideoLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(courseVideoLst,HttpStatus.NOT_FOUND);
		}
	}
}
