/**
 * 
 */
package com.incus.quizapp.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incus.quizapp.model.CourseInput;
import com.incus.quizapp.model.Deck;
import com.incus.quizapp.model.DeckLst;
import com.incus.quizapp.model.IdInput;
import com.incus.quizapp.model.TitleInput;
import com.incus.quizapp.service.DeckService;
import com.incus.quizapp.util.Constants;

import io.swagger.annotations.ApiOperation;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
@RestController
@RequestMapping("/flashcardDeck")
public class DeckController {
	@Autowired
	DeckService deckService;
	
	private static final Logger logger = LoggerFactory.getLogger(DeckController.class);
	
	@ApiOperation(value="Add Deck")
	@PostMapping(value="/add")
	public ResponseEntity<?> addDeck(@RequestBody Deck deck){
		logger.debug("In Add Deck");
		DeckLst deckLst = new DeckLst();
		deckLst.setState(false);
		if(deck.getDeckTitle()!=null && !deck.getDeckTitle().trim().isEmpty()) {
			deck.setCreatedDate(LocalDateTime.now());
			deck.setId(null);
			deck.setModifiedDate(null);
			deck.setModifiedBy(null);
			Deck deckAdded = deckService.addDeck(deck);
			List<Deck> lstDeck = new ArrayList();
			lstDeck.add(deckAdded);
			deckLst.setDeckList(lstDeck);
			deckLst.setState(true);
			deckLst.setMessage("SUCCESS");
			if(deckAdded!=null) {
				return new ResponseEntity<>(deckLst,HttpStatus.OK);
			}else {
				deckLst.setMessage(Constants.ERR_CPT_001);
				return new ResponseEntity<>(deckLst,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			deckLst.setMessage(Constants.ERR_CPT_004);
			return new ResponseEntity<>(deckLst,HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value="Update Deck")
	@PostMapping(value="/update")
	public ResponseEntity<?> updateDeck(@RequestBody Deck deck){
		logger.debug("In Update Deck");
		DeckLst deckLst = new DeckLst();
		deckLst.setState(false);
		if(deck.getDeckTitle()!=null && !deck.getDeckTitle().trim().isEmpty()) {
			deck.setModifiedDate(LocalDateTime.now());
			Deck deckUpdated = deckService.updateDeck(deck);
			List<Deck> lstDeck = new ArrayList();
			lstDeck.add(deckUpdated);
			deckLst.setDeckList(lstDeck);
			deckLst.setState(true);
			deckLst.setMessage("SUCCESS");
			if(deckUpdated!=null) {
				return new ResponseEntity<>(deckLst,HttpStatus.OK);
			}else {
				deckLst.setMessage(Constants.ERR_CPT_001);
				return new ResponseEntity<>(deckLst,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			deckLst.setMessage(Constants.ERR_CPT_004);
			return new ResponseEntity<>(Constants.ERR_CPT_004,HttpStatus.BAD_REQUEST);
		}
	}
	@ApiOperation(value="Get Deck")
	@PostMapping(value="/getDeck")
	public ResponseEntity<?> getDeck(@RequestBody IdInput deckId){
		logger.debug("In Get Deck By Id");
		DeckLst deckLst = new DeckLst();
		
		Deck extDeck = deckService.getDeck(deckId.getId());
		if(extDeck!=null) {
			List<Deck> lstDeck = new ArrayList();
			lstDeck.add(extDeck);
			deckLst.setDeckList(lstDeck);
			deckLst.setState(true);
			deckLst.setMessage("SUCCESS");
			return new ResponseEntity<>(deckLst,HttpStatus.OK);
		}else {
			deckLst.setState(false);
			deckLst.setMessage(Constants.ERR_CPT_002);
			return new ResponseEntity<>(deckLst,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Deck By Title")
	@PostMapping(value="/getDeckByTitle")
	public ResponseEntity<?> getDeckByTitle(@RequestBody TitleInput deckTitle){
		logger.debug("In Get Deck by Title");
		DeckLst deckLst = new DeckLst();
		List<Deck> extDeckLst = deckService.getDeckByDeckTitle(deckTitle.getTitle());
		if(extDeckLst!=null && extDeckLst.size()>0) {
			deckLst.setDeckList(extDeckLst);
			deckLst.setState(true);
			deckLst.setMessage("SUCCESS");
			return new ResponseEntity<>(deckLst,HttpStatus.OK);
		}else {
			deckLst.setState(false);
			deckLst.setMessage(Constants.ERR_CPT_002);
			return new ResponseEntity<>(deckLst,HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation(value="Get All Decks")
	@PostMapping(value="/getAll")
	public ResponseEntity<?> getAll(){
		logger.debug("In Get All Decks");
		List<Deck> lstDeck = deckService.getAllDecks();
		DeckLst deckLst = new DeckLst();
		deckLst.setDeckList(lstDeck);
		deckLst.setState(true);
		deckLst.setMessage("SUCCESS");
 		if(lstDeck!=null) {
			return new ResponseEntity<>(deckLst,HttpStatus.OK);
		}else {
			deckLst.setState(false);
			deckLst.setMessage(Constants.ERR_CPT_002);
			return new ResponseEntity<>(deckLst,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Deck By Flashcard Course")
	@PostMapping(value="/getDeckByFlashcardCourse")
	public ResponseEntity<?> getDeckByFlashcardCourse(@RequestBody CourseInput flashcardCourse){
		logger.debug("In Get Deck By Flashcard Course");
		DeckLst deckLst = new DeckLst();

		List<Deck> extDeckLst = deckService.getByFlashcardCourse(flashcardCourse.getCourse());
		if(extDeckLst!=null && extDeckLst.size()>0) {
			deckLst.setDeckList(extDeckLst);
			deckLst.setState(true);
			deckLst.setMessage("SUCCESS");
			return new ResponseEntity<>(deckLst,HttpStatus.OK);
		}else {
			deckLst.setState(false);
			deckLst.setMessage(Constants.ERR_CPT_002);
			return new ResponseEntity<>(deckLst,HttpStatus.NOT_FOUND);
		}
	}
}
