/**
 * 
 */
package com.incus.quizapp.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incus.quizapp.model.CountryInput;
import com.incus.quizapp.model.ExamCategory;
import com.incus.quizapp.model.ExamCategoryLst;
import com.incus.quizapp.model.IdInput;
import com.incus.quizapp.model.NameInput;
import com.incus.quizapp.service.ExamCategoryService;
import com.incus.quizapp.util.Constants;

import io.swagger.annotations.ApiOperation;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 19-Dec-2020
 * Description:
 * *******************************************************
 */
@RestController
@RequestMapping("/examCategory")
public class ExamCategoryController {
	@Autowired
	ExamCategoryService examCategoryService;
	private static final Logger logger = LoggerFactory.getLogger(ExamCategoryController.class);
	
	@ApiOperation(value="Add ExamCategory")
	@PostMapping(value="/add")
	public ResponseEntity<?> addExamCategory(@RequestBody ExamCategory examCategory){
		logger.debug("In Add ExamCategory");
		ExamCategoryLst examCategoryLst = new ExamCategoryLst();
		examCategoryLst.setState(false);
		if(examCategory.getCategoryName()!=null && !examCategory.getCategoryName().trim().isEmpty()) {
			examCategory.setCreatedDate(LocalDateTime.now());
			examCategory.setId(null);
			examCategory.setModifiedDate(null);
			examCategory.setModifiedBy(null);
			ExamCategory examCategoryAdded = examCategoryService.addExamCategory(examCategory);
			List<ExamCategory> lstExamCategory = new ArrayList();
			lstExamCategory.add(examCategoryAdded);
			examCategoryLst.setExamCategoryList(lstExamCategory);
			examCategoryLst.setState(true);
			examCategoryLst.setMessage("SUCCESS");
			if(examCategoryAdded!=null) {
				return new ResponseEntity<>(examCategoryLst,HttpStatus.OK);
			}else {
				examCategoryLst.setMessage(Constants.ERR_CRS_001);
				return new ResponseEntity<>(examCategoryLst,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			examCategoryLst.setMessage(Constants.ERR_CRS_004);
			return new ResponseEntity<>(Constants.ERR_CRS_004,HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value="Update ExamCategory")
	@PostMapping(value="/update")
	public ResponseEntity<?> updateExamCategory(@RequestBody ExamCategory examCategory){
		logger.debug("In Update ExamCategory");
		ExamCategoryLst examCategoryLst = new ExamCategoryLst();
		examCategoryLst.setState(false);
		if(examCategory.getCategoryName()!=null && !examCategory.getCategoryName().trim().isEmpty()) {
			examCategory.setModifiedDate(LocalDateTime.now());
			ExamCategory examCategoryUpdated = examCategoryService.updateExamCategory(examCategory);
			List<ExamCategory> lstExamCategory = new ArrayList();
			lstExamCategory.add(examCategoryUpdated);
			examCategoryLst.setExamCategoryList(lstExamCategory);
			examCategoryLst.setState(true);
			examCategoryLst.setMessage("SUCCESS");
			if(examCategoryUpdated!=null) {
				return new ResponseEntity<>(examCategoryLst,HttpStatus.OK);
			}else {
				examCategoryLst.setMessage(Constants.ERR_CRS_001);
				return new ResponseEntity<>(examCategoryLst,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			examCategoryLst.setMessage(Constants.ERR_CRS_004);
			return new ResponseEntity<>(examCategoryLst,HttpStatus.BAD_REQUEST);
		}
	}
	@ApiOperation(value="Get ExamCategory")
	@PostMapping(value="/getExamCategory")
	public ResponseEntity<?> getExamCategory(@RequestBody IdInput examCategoryId){
		logger.debug("In Get ExamCategory By Id");
		ExamCategoryLst examCategoryLst = new ExamCategoryLst();
		ExamCategory extExamCategory = examCategoryService.getExamCategoryById(examCategoryId.getId());
		if(extExamCategory!=null) {
			List<ExamCategory> lstExamCategory = new ArrayList();
			lstExamCategory.add(extExamCategory);
			examCategoryLst.setExamCategoryList(lstExamCategory);
			examCategoryLst.setState(true);
			examCategoryLst.setMessage("SUCCESS");
			return new ResponseEntity<>(examCategoryLst,HttpStatus.OK);
		}else {
			examCategoryLst.setState(false);
			examCategoryLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(examCategoryLst,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get ExamCategory By Name")
	@PostMapping(value="/getExamCategoryByName")
	public ResponseEntity<?> getExamCategoryByName(@RequestBody NameInput examCategoryName){
		logger.debug("In Get ExamCategory by Name");
		ExamCategoryLst examCategoryLst = new ExamCategoryLst();
		examCategoryLst.setState(false);
		List<ExamCategory> extExamCategoryLst = examCategoryService.getExamCategoryByCategoryName(examCategoryName.getName());
		if(extExamCategoryLst!=null && extExamCategoryLst.size()>0) {
			examCategoryLst.setExamCategoryList(extExamCategoryLst);
			examCategoryLst.setState(true);
			examCategoryLst.setMessage("SUCCESS");
			return new ResponseEntity<>(extExamCategoryLst.get(0),HttpStatus.OK);
		}else {
			examCategoryLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(examCategoryLst,HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation(value="Get ExamCategory By Country")
	@PostMapping(value="/getExamCategoryByCountry")
	public ResponseEntity<?> getExamCategoryByCountry(@RequestBody CountryInput country){
		logger.debug("In Get ExamCategory by Country");
		ExamCategoryLst examCategoryLst = new ExamCategoryLst();
		examCategoryLst.setState(false);
		List<ExamCategory> extExamCategoryLst = examCategoryService.getExamCategoryByCountry(country.getCountry());
		if(extExamCategoryLst!=null) {
			examCategoryLst.setExamCategoryList(extExamCategoryLst);
			examCategoryLst.setState(true);
			examCategoryLst.setMessage("SUCCESS");
			return new ResponseEntity<>(examCategoryLst,HttpStatus.OK);
		}else {
			examCategoryLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(examCategoryLst,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get All ExamCategorys")
	@PostMapping(value="/getAll")
	public ResponseEntity<?> getAll(){
		logger.debug("In Get All ExamCategorys");
		List<ExamCategory> lstExamCategory = examCategoryService.getAllExamCategorys();
		ExamCategoryLst examCategoryLst = new ExamCategoryLst();
		examCategoryLst.setExamCategoryList(lstExamCategory);
		examCategoryLst.setState(true);
		examCategoryLst.setMessage("SUCCESS");
 		if(lstExamCategory!=null) {
			return new ResponseEntity<>(examCategoryLst,HttpStatus.OK);
		}else {
			examCategoryLst.setState(false);
			examCategoryLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(examCategoryLst,HttpStatus.NOT_FOUND);
		}
	}
}
