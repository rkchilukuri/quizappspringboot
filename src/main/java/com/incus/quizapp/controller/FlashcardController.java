/**
 * 
 */
package com.incus.quizapp.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incus.quizapp.model.Flashcard;
import com.incus.quizapp.model.FlashcardLst;
import com.incus.quizapp.model.IdInput;
import com.incus.quizapp.model.NameInput;
import com.incus.quizapp.model.TitleInput;
import com.incus.quizapp.service.FlashcardService;
import com.incus.quizapp.util.Constants;

import io.swagger.annotations.ApiOperation;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 22-Dec-2020
 * Description:
 * *******************************************************
 */
@RestController
@RequestMapping("/flashcard")
public class FlashcardController {
	@Autowired
	FlashcardService flashcardService;
	
	private static final Logger logger = LoggerFactory.getLogger(FlashcardController.class);
	
	@ApiOperation(value="Add Flashcard Flashcard")
	@PostMapping(value="/add")
	public ResponseEntity<?> addFlashcard(@RequestBody Flashcard flashcard){
		logger.debug("In Add Flashcard Flashcard");
		if(flashcard.getFrontText()!=null && !flashcard.getFrontText().trim().isEmpty()) {
			flashcard.setCreatedDate(LocalDateTime.now());
			flashcard.setId(null);
			flashcard.setModifiedDate(null);
			flashcard.setModifiedBy(null);
			Flashcard flashcardAdded = flashcardService.addFlashcard(flashcard);
			List<Flashcard> lstFlashcard = new ArrayList();
			lstFlashcard.add(flashcardAdded);
			FlashcardLst flashcardLst = new FlashcardLst();
			flashcardLst.setFlashcardList(lstFlashcard);
			flashcardLst.setState(true);
			flashcardLst.setMessage("SUCCESS");
			if(flashcardAdded!=null) {
				return new ResponseEntity<>(flashcardLst,HttpStatus.OK);
			}else {
				return new ResponseEntity<>(Constants.ERR_VCRS_001,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(Constants.ERR_VCRS_004,HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value="Update Flashcard")
	@PostMapping(value="/update")
	public ResponseEntity<?> updateFlashcard(@RequestBody Flashcard flashcard){
		logger.debug("In Update Flashcard");
		if(flashcard.getFrontText()!=null && !flashcard.getFrontText().trim().isEmpty()) {
			flashcard.setModifiedDate(LocalDateTime.now());
			Flashcard flashcardUpdated = flashcardService.updateFlashcard(flashcard);
			List<Flashcard> lstFlashcard = new ArrayList();
			lstFlashcard.add(flashcardUpdated);
			FlashcardLst flashcardLst = new FlashcardLst();
			flashcardLst.setFlashcardList(lstFlashcard);
			flashcardLst.setState(true);
			flashcardLst.setMessage("SUCCESS");
			if(flashcardUpdated!=null) {
				return new ResponseEntity<>(flashcardLst,HttpStatus.OK);
			}else {
				return new ResponseEntity<>(Constants.ERR_VCRS_001,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(Constants.ERR_VCRS_004,HttpStatus.BAD_REQUEST);
		}
	}
	@ApiOperation(value="Get Flashcard")
	@PostMapping(value="/getFlashcard")
	public ResponseEntity<?> getFlashcard(@RequestBody IdInput flashcardId){
		logger.debug("In Get Flashcard By Id");
		Flashcard extFlashcard = flashcardService.getFlashcard(flashcardId.getId());
		if(extFlashcard!=null) {
			return new ResponseEntity<>(extFlashcard,HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_VCRS_002,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Flashcard By Title")
	@PostMapping(value="/getFlashcardByTitle")
	public ResponseEntity<?> getFlashcardByTitle(@RequestBody TitleInput flashcardTitle){
		logger.debug("In Get Flashcard by Title");
		List<Flashcard> extFlashcardLst = flashcardService.getFlashcardByFlashcardTitle(flashcardTitle.getTitle());
		if(extFlashcardLst!=null && extFlashcardLst.size()>0) {
			return new ResponseEntity<>(extFlashcardLst.get(0),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_VCRS_002,HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation(value="Get All Flashcards")
	@PostMapping(value="/getAll")
	public ResponseEntity<?> getAll(){
		logger.debug("In Get All Flashcards");
		List<Flashcard> lstFlashcard = flashcardService.getAllFlashcards();
		FlashcardLst flashcardLst = new FlashcardLst();
		flashcardLst.setFlashcardList(lstFlashcard);
		flashcardLst.setState(true);
		flashcardLst.setMessage("SUCCESS");
 		if(lstFlashcard!=null) {
			return new ResponseEntity<>(flashcardLst,HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_VCRS_002,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Flashcard By Deck")
	@PostMapping(value="/getFlashcardByDeck")
	public ResponseEntity<?> getFlashcardByDeck(@RequestBody NameInput deck){
		logger.debug("In Get Flashcard by Deck");
		List<Flashcard> extFlashcardLst = flashcardService.getFlashcardByDeck(deck.getName());
		if(extFlashcardLst!=null && extFlashcardLst.size()>0) {
			return new ResponseEntity<>(extFlashcardLst.get(0),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_VCRS_002,HttpStatus.NOT_FOUND);
		}
	}
}
