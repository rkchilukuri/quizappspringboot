/**
 * 
 */
package com.incus.quizapp.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incus.quizapp.model.Course;
import com.incus.quizapp.model.CourseLst;
import com.incus.quizapp.model.CreatedbyInput;
import com.incus.quizapp.model.ExamCategoryInput;
import com.incus.quizapp.model.IdInput;
import com.incus.quizapp.model.LiveclassCourse;
import com.incus.quizapp.model.LiveclassCourseLst;
import com.incus.quizapp.model.TitleInput;
import com.incus.quizapp.service.LiveclassCourseService;
import com.incus.quizapp.util.Constants;

import io.swagger.annotations.ApiOperation;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
@RestController
@RequestMapping("/liveclassCourse")
public class LiveClassCourseController {
	@Autowired
	LiveclassCourseService courseService;
	
	private static final Logger logger = LoggerFactory.getLogger(LiveClassCourseController.class);
	
	@ApiOperation(value="Add Liveclass Course")
	@PostMapping(value="/add")
	public ResponseEntity<?> addLiveclassCourse(@RequestBody LiveclassCourse course){
		logger.debug("In Add Liveclass Course");
		if(course.getLiveclassTitle()!=null && !course.getLiveclassTitle().trim().isEmpty()) {
			course.setCreatedDate(LocalDateTime.now());
			course.setId(null);
			course.setModifiedDate(null);
			course.setModifiedBy(null);
			LiveclassCourse courseAdded = courseService.addCourse(course);
			List<LiveclassCourse> lstCourse = new ArrayList();
			lstCourse.add(courseAdded);
			LiveclassCourseLst courseLst = new LiveclassCourseLst();
			courseLst.setLiveclassCourseList(lstCourse);
			courseLst.setState(true);
			courseLst.setMessage("SUCCESS");
			if(courseAdded!=null) {
				return new ResponseEntity<>(courseLst,HttpStatus.OK);
			}else {
				return new ResponseEntity<>(Constants.ERR_LCRS_001,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(Constants.ERR_LCRS_004,HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value="Update Course")
	@PostMapping(value="/update")
	public ResponseEntity<?> updateCourse(@RequestBody LiveclassCourse course){
		logger.debug("In Update Course");
		if(course.getLiveclassTitle()!=null && !course.getLiveclassTitle().trim().isEmpty()) {
			course.setModifiedDate(LocalDateTime.now());
			LiveclassCourse courseUpdated = courseService.updateCourse(course);
			List<LiveclassCourse> lstCourse = new ArrayList();
			lstCourse.add(courseUpdated);
			LiveclassCourseLst courseLst = new LiveclassCourseLst();
			courseLst.setLiveclassCourseList(lstCourse);
			courseLst.setState(true);
			courseLst.setMessage("SUCCESS");
			if(courseUpdated!=null) {
				return new ResponseEntity<>(courseLst,HttpStatus.OK);
			}else {
				return new ResponseEntity<>(Constants.ERR_LCRS_001,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(Constants.ERR_LCRS_004,HttpStatus.BAD_REQUEST);
		}
	}
	@ApiOperation(value="Get Course")
	@PostMapping(value="/getCourse")
	public ResponseEntity<?> getCourse(@RequestBody IdInput courseId){
		logger.debug("In Get Course By Id");
		LiveclassCourse extCourse = courseService.getCourse(courseId.getId());
		if(extCourse!=null) {
			return new ResponseEntity<>(extCourse,HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_LCRS_002,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Course By Title")
	@PostMapping(value="/getCourseByTitle")
	public ResponseEntity<?> getCourseByTitle(@RequestBody TitleInput courseTitle){
		logger.debug("In Get Course by Title");
		List<LiveclassCourse> extCourseLst = courseService.getCourseByCourseTitle(courseTitle.getTitle());
		if(extCourseLst!=null && extCourseLst.size()>0) {
			return new ResponseEntity<>(extCourseLst.get(0),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_LCRS_002,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Course By Exam Category")
	@PostMapping(value="/getCourseByExamCategory")
	public ResponseEntity<?> getCourseByExamCategory(@RequestBody ExamCategoryInput examCategory){
		logger.debug("In Get Video Course by Exam Category");
		List<LiveclassCourse> extCourseLst = courseService.getCourseByExamCategory(examCategory.getExamCategory());
		if(extCourseLst!=null && extCourseLst.size()>0) {
			return new ResponseEntity<>(extCourseLst.get(0),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_LCRS_002,HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation(value="Get All Courses")
	@PostMapping(value="/getAll")
	public ResponseEntity<?> getAll(){
		logger.debug("In Get All Courses");
		List<LiveclassCourse> lstCourse = courseService.getAllCourses();
		LiveclassCourseLst courseLst = new LiveclassCourseLst();
		courseLst.setLiveclassCourseList(lstCourse);
		courseLst.setState(true);
		courseLst.setMessage("SUCCESS");
 		if(lstCourse!=null) {
			return new ResponseEntity<>(courseLst,HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_LCRS_002,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Course By Creator")
	@PostMapping(value="/getCourseByCreator")
	public ResponseEntity<?> getCourseByCreator(@RequestBody CreatedbyInput createdBy){
		logger.debug("In Get Course by Title");
		CourseLst courseLst = new CourseLst();
		courseLst.setState(false);
		List<Course> extCourseLst = courseService.getCourseByCreator(createdBy.getCreatedBy());
		if(extCourseLst!=null && extCourseLst.size()>0) {
			courseLst.setCourseList(extCourseLst);
			courseLst.setState(true);
			courseLst.setMessage("SUCCESS");
			return new ResponseEntity<>(extCourseLst.get(0),HttpStatus.OK);
		}else {
			courseLst.setMessage(Constants.ERR_LCRS_002);
			return new ResponseEntity<>(courseLst,HttpStatus.NOT_FOUND);
		}
	}
}
