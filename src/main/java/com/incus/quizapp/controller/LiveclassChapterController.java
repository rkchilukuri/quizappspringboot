/**
 * 
 */
package com.incus.quizapp.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incus.quizapp.model.CourseInput;
import com.incus.quizapp.model.IdInput;
import com.incus.quizapp.model.LiveclassChapter;
import com.incus.quizapp.model.LiveclassChapterLst;
import com.incus.quizapp.model.TitleInput;
import com.incus.quizapp.service.LiveclassChapterService;
import com.incus.quizapp.util.Constants;

import io.swagger.annotations.ApiOperation;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
@RestController
@RequestMapping("/liveclassLiveclassChapter")
public class LiveclassChapterController {
	@Autowired
	LiveclassChapterService chapterService;
	
	private static final Logger logger = LoggerFactory.getLogger(LiveclassChapterController.class);
	
	@ApiOperation(value="Add Liveclass LiveclassChapter")
	@PostMapping(value="/add")
	public ResponseEntity<?> addLiveclassChapter(@RequestBody LiveclassChapter chapter){
		logger.debug("In Add LiveclassChapter");
		if(chapter.getLiveclassChapterTitle()!=null && !chapter.getLiveclassChapterTitle().trim().isEmpty()) {
			chapter.setCreatedDate(LocalDateTime.now());
			chapter.setId(null);
			chapter.setModifiedDate(null);
			chapter.setModifiedBy(null);
			LiveclassChapter chapterAdded = chapterService.addLiveclassChapter(chapter);
			List<LiveclassChapter> lstLiveclassChapter = new ArrayList();
			lstLiveclassChapter.add(chapterAdded);
			LiveclassChapterLst chapterLst = new LiveclassChapterLst();
			chapterLst.setLiveclassChapterList(lstLiveclassChapter);
			chapterLst.setState(true);
			chapterLst.setMessage("SUCCESS");
			if(chapterAdded!=null) {
				return new ResponseEntity<>(chapterLst,HttpStatus.OK);
			}else {
				return new ResponseEntity<>(Constants.ERR_CPT_001,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(Constants.ERR_CPT_004,HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value="Update LiveclassChapter")
	@PostMapping(value="/update")
	public ResponseEntity<?> updateLiveclassChapter(@RequestBody LiveclassChapter chapter){
		logger.debug("In Update LiveclassChapter");
		if(chapter.getLiveclassChapterTitle()!=null && !chapter.getLiveclassChapterTitle().trim().isEmpty()) {
			chapter.setModifiedDate(LocalDateTime.now());
			LiveclassChapter chapterUpdated = chapterService.updateLiveclassChapter(chapter);
			List<LiveclassChapter> lstLiveclassChapter = new ArrayList();
			lstLiveclassChapter.add(chapterUpdated);
			LiveclassChapterLst chapterLst = new LiveclassChapterLst();
			chapterLst.setLiveclassChapterList(lstLiveclassChapter);
			chapterLst.setState(true);
			chapterLst.setMessage("SUCCESS");
			if(chapterUpdated!=null) {
				return new ResponseEntity<>(chapterLst,HttpStatus.OK);
			}else {
				return new ResponseEntity<>(Constants.ERR_CPT_001,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(Constants.ERR_CPT_004,HttpStatus.BAD_REQUEST);
		}
	}
	@ApiOperation(value="Get LiveclassChapter")
	@PostMapping(value="/getLiveclassChapter")
	public ResponseEntity<?> getLiveclassChapter(@RequestBody IdInput chapterId){
		logger.debug("In Get LiveclassChapter By Id");
		LiveclassChapter extLiveclassChapter = chapterService.getLiveclassChapter(chapterId.getId());
		if(extLiveclassChapter!=null) {
			return new ResponseEntity<>(extLiveclassChapter,HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_CPT_002,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get LiveclassChapter By Title")
	@PostMapping(value="/getLiveclassChapterByTitle")
	public ResponseEntity<?> getLiveclassChapterByTitle(@RequestBody TitleInput chapterTitle){
		logger.debug("In Get LiveclassChapter by Title");
		List<LiveclassChapter> extLiveclassChapterLst = chapterService.getLiveclassChapterByLiveclassChapterTitle(chapterTitle.getTitle());
		if(extLiveclassChapterLst!=null && extLiveclassChapterLst.size()>0) {
			return new ResponseEntity<>(extLiveclassChapterLst.get(0),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_CPT_002,HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation(value="Get All LiveclassChapters")
	@PostMapping(value="/getAll")
	public ResponseEntity<?> getAll(){
		logger.debug("In Get All LiveclassChapters");
		List<LiveclassChapter> lstLiveclassChapter = chapterService.getAllLiveclassChapters();
		LiveclassChapterLst chapterLst = new LiveclassChapterLst();
		chapterLst.setLiveclassChapterList(lstLiveclassChapter);
		chapterLst.setState(true);
		chapterLst.setMessage("SUCCESS");
 		if(lstLiveclassChapter!=null) {
			return new ResponseEntity<>(chapterLst,HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_CPT_002,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get LiveclassChapter By Course")
	@PostMapping(value="/getLiveclassChapterByCourse")
	public ResponseEntity<?> getLiveclassChapterByCourse(@RequestBody CourseInput course){
		logger.debug("In Get LiveclassChapter by Title");
		List<LiveclassChapter> extLiveclassChapterLst = chapterService.getLiveclassChapterByCourse(course.getCourse());
		if(extLiveclassChapterLst!=null && extLiveclassChapterLst.size()>0) {
			return new ResponseEntity<>(extLiveclassChapterLst.get(0),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_CPT_002,HttpStatus.NOT_FOUND);
		}
	}
}
