/**
 * 
 */
package com.incus.quizapp.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incus.quizapp.model.Chapter;
import com.incus.quizapp.model.ChapterLst;
import com.incus.quizapp.model.TitleInput;
import com.incus.quizapp.model.CourseInput;
import com.incus.quizapp.model.IdInput;
import com.incus.quizapp.model.ValidChapter;
import com.incus.quizapp.service.ChapterService;
import com.incus.quizapp.util.Constants;

import io.swagger.annotations.ApiOperation;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
@RestController
@RequestMapping("/chapter")
public class QuizChapterController {

	@Autowired
	ChapterService chapterService;
	private static final Logger logger = LoggerFactory.getLogger(QuizChapterController.class);
	
	@ApiOperation(value="Add Chapter")
	@PostMapping(value="/add")
	public ResponseEntity<?> addChapter(@RequestBody Chapter chapter){
		logger.debug("In Add Chapter");
		ChapterLst chapterLst = new ChapterLst();
		chapterLst.setState(false);
		if(chapter.getChapterTitle()!=null && !chapter.getChapterTitle().trim().isEmpty()) {
			chapter.setCreatedDate(LocalDateTime.now());
			chapter.setId(null);
			chapter.setModifiedDate(null);
			chapter.setModifiedBy(null);
			Chapter chapterAdded = chapterService.addChapter(chapter);
			List<Chapter> lstChapter = new ArrayList();
			lstChapter.add(chapterAdded);
			chapterLst.setChapterList(lstChapter);
			chapterLst.setState(true);
			chapterLst.setMessage("SUCCESS");
			if(chapterAdded!=null) {
				return new ResponseEntity<>(chapterLst,HttpStatus.OK);
			}else {
				chapterLst.setMessage(Constants.ERR_CPT_001);
				return new ResponseEntity<>(chapterLst,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			chapterLst.setMessage(Constants.ERR_CPT_004);
			return new ResponseEntity<>(chapterLst,HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value="Update Chapter")
	@PostMapping(value="/update")
	public ResponseEntity<?> updateChapter(@RequestBody Chapter chapter){
		logger.debug("In Update Chapter");
		ChapterLst chapterLst = new ChapterLst();
		chapterLst.setState(false);
		if(chapter.getChapterTitle()!=null && !chapter.getChapterTitle().trim().isEmpty()) {
			chapter.setModifiedDate(LocalDateTime.now());
			Chapter chapterUpdated = chapterService.updateChapter(chapter);
			List<Chapter> lstChapter = new ArrayList();
			lstChapter.add(chapterUpdated);
			chapterLst.setChapterList(lstChapter);
			chapterLst.setState(true);
			chapterLst.setMessage("SUCCESS");
			if(chapterUpdated!=null) {
				return new ResponseEntity<>(chapterLst,HttpStatus.OK);
			}else {
				chapterLst.setMessage(Constants.ERR_CPT_001);
				return new ResponseEntity<>(chapterLst,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			chapterLst.setMessage(Constants.ERR_CPT_004);
			return new ResponseEntity<>(chapterLst,HttpStatus.BAD_REQUEST);
		}
	}
	@ApiOperation(value="Get Chapter")
	@PostMapping(value="/getChapter")
	public ResponseEntity<?> getChapter(@RequestBody IdInput chapterId){
		logger.debug("In Get Chapter By Id");
		ChapterLst chapterLst = new ChapterLst();
		chapterLst.setState(false);
		Chapter extChapter = chapterService.getChapter(chapterId.getId());
		if(extChapter!=null) {
			List<Chapter> lstChapter = new ArrayList();
			lstChapter.add(extChapter);
			chapterLst.setChapterList(lstChapter);
			chapterLst.setState(true);
			chapterLst.setMessage("SUCCESS");
			return new ResponseEntity<>(chapterLst,HttpStatus.OK);
		}else {
			chapterLst.setMessage(Constants.ERR_CPT_002);
			return new ResponseEntity<>(chapterLst,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Chapter By Title")
	@PostMapping(value="/getChapterByTitle")
	public ResponseEntity<?> getChapterByTitle(@RequestBody TitleInput chapterTitle){
		logger.debug("In Get Chapter by Title");
		ChapterLst chapterLst = new ChapterLst();
		chapterLst.setState(false);
		List<Chapter> extChapterLst = chapterService.getChapterByChapterTitle(chapterTitle.getTitle());
		if(extChapterLst!=null && extChapterLst.size()>0) {
			chapterLst.setChapterList(extChapterLst);
			chapterLst.setState(true);
			chapterLst.setMessage("SUCCESS");
			return new ResponseEntity<>(chapterLst,HttpStatus.OK);
		}else {
			chapterLst.setMessage(Constants.ERR_CPT_002);
			return new ResponseEntity<>(chapterLst,HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation(value="Get All Chapters")
	@PostMapping(value="/getAll")
	public ResponseEntity<?> getAll(){
		logger.debug("In Get All Chapters");
		List<Chapter> lstChapter = chapterService.getAllChapters();
		ChapterLst chapterLst = new ChapterLst();
		chapterLst.setChapterList(lstChapter);
		chapterLst.setState(false);
		chapterLst.setMessage("FAIL");
 		if(lstChapter!=null && lstChapter.size()>0) {
 			chapterLst.setState(true);
 			chapterLst.setMessage("SUCCESS");
			return new ResponseEntity<>(chapterLst,HttpStatus.OK);
		}else {
			chapterLst.setMessage(Constants.ERR_CPT_002);
			return new ResponseEntity<>(chapterLst,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Chapter By Course")
	@PostMapping(value="/getChapterByCourse")
	public ResponseEntity<?> getChapterByCourse(@RequestBody CourseInput courseName){
		logger.debug("In Get Chapter by Course");
		ChapterLst chapterLst = new ChapterLst();
		chapterLst.setState(false);
		chapterLst.setMessage("FAIL");
		List<Chapter> extChapterLst = chapterService.getChapterByCourse(courseName.getCourse());
		if(extChapterLst!=null && extChapterLst.size()>0) {
			chapterLst.setChapterList(extChapterLst);
			chapterLst.setState(true);
			chapterLst.setMessage("SUCCESS");
			return new ResponseEntity<>(extChapterLst,HttpStatus.OK);
		}else {
			chapterLst.setMessage(Constants.ERR_CPT_002);
			return new ResponseEntity<>(chapterLst,HttpStatus.NOT_FOUND);
		}
	}

	public ValidChapter validateChapter(Chapter chapter) {
		ValidChapter validChapter = new ValidChapter();
		List<Chapter> extChapterLst = chapterService.getChapterByChapterTitle(chapter.getChapterTitle());
		if(extChapterLst!=null && extChapterLst.size()>0)
		{
			validChapter.setChapter(extChapterLst.get(0));
			validChapter.setValidationMsg(Constants.ERR_CPT_003);
		}
		return validChapter;
	}
}
