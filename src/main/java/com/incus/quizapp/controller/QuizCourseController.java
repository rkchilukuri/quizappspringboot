/**
 * 
 */
package com.incus.quizapp.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incus.quizapp.model.Course;
import com.incus.quizapp.model.CourseLst;
import com.incus.quizapp.model.CreatedbyInput;
import com.incus.quizapp.model.ExamCategoryInput;
import com.incus.quizapp.model.IdInput;
import com.incus.quizapp.model.TitleInput;
import com.incus.quizapp.model.ValidCourse;
import com.incus.quizapp.service.CourseService;
import com.incus.quizapp.util.Constants;

import io.swagger.annotations.ApiOperation;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */

@RestController
@RequestMapping("/quizCourse")
public class QuizCourseController {

	@Autowired
	CourseService courseService;
	private static final Logger logger = LoggerFactory.getLogger(QuizCourseController.class);
	
	@ApiOperation(value="Add Course")
	@PostMapping(value="/add")
	public ResponseEntity<?> addCourse(@RequestBody Course course){
		logger.debug("In Add Course");
		CourseLst courseLst = new CourseLst();
		courseLst.setState(false);
		if(course.getCourseTitle()!=null && !course.getCourseTitle().trim().isEmpty()) {
		//	ValidCourse validCourse = validateCourse(course);
		//	if(validCourse.getCourse()==null) {
			course.setCreatedDate(LocalDateTime.now());
			course.setId(null);
			course.setModifiedDate(null);
			course.setModifiedBy(null);
			Course courseAdded = courseService.addCourse(course);
			List<Course> lstCourse = new ArrayList();
			lstCourse.add(courseAdded);
			courseLst.setCourseList(lstCourse);
			courseLst.setState(true);
			courseLst.setMessage("SUCCESS");
			if(courseAdded!=null) {
				return new ResponseEntity<>(courseLst,HttpStatus.OK);
			}else {
				courseLst.setMessage(Constants.ERR_CRS_001);
				return new ResponseEntity<>(courseLst,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		/*	}else {
				return new ResponseEntity<>(validCourse.getValidationMsg(),HttpStatus.BAD_REQUEST);
			}*/
		}else {
			courseLst.setMessage(Constants.ERR_CRS_004);
			return new ResponseEntity<>(courseLst,HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value="Update Course")
	@PostMapping(value="/update")
	public ResponseEntity<?> updateCourse(@RequestBody Course course){
		logger.debug("In Update Course");
		CourseLst courseLst = new CourseLst();
		courseLst.setState(false);
		if(course.getCourseTitle()!=null && !course.getCourseTitle().trim().isEmpty()) {
			course.setModifiedDate(LocalDateTime.now());
			Course courseUpdated = courseService.updateCourse(course);
			List<Course> lstCourse = new ArrayList();
			lstCourse.add(courseUpdated);
			courseLst.setCourseList(lstCourse);
			courseLst.setState(true);
			courseLst.setMessage("SUCCESS");
			if(courseUpdated!=null) {
				return new ResponseEntity<>(courseLst,HttpStatus.OK);
			}else {
				courseLst.setMessage(Constants.ERR_CRS_001);
				return new ResponseEntity<>(courseLst,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			courseLst.setMessage(Constants.ERR_CRS_004);
			return new ResponseEntity<>(courseLst,HttpStatus.BAD_REQUEST);
		}
	}
	@ApiOperation(value="Get Course")
	@PostMapping(value="/getCourse")
	public ResponseEntity<?> getCourse(@RequestBody IdInput courseId){
		logger.debug("In Get Course By Id");
		CourseLst courseLst = new CourseLst();
		courseLst.setState(false);
		Course extCourse = courseService.getCourse(courseId.getId());
		if(extCourse!=null) {
			List<Course> lstCourse = new ArrayList();
			lstCourse.add(extCourse);
			courseLst.setCourseList(lstCourse);
			courseLst.setState(true);
			courseLst.setMessage("SUCCESS");
			return new ResponseEntity<>(courseLst,HttpStatus.OK);
		}else {
			courseLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(courseLst,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Course By Title")
	@PostMapping(value="/getCourseByTitle")
	public ResponseEntity<?> getCourseByTitle(@RequestBody TitleInput courseTitle){
		logger.debug("In Get Course by Title");
		CourseLst courseLst = new CourseLst();
		courseLst.setState(false);
		List<Course> extCourseLst = courseService.getCourseByCourseTitle(courseTitle.getTitle());
		if(extCourseLst!=null && extCourseLst.size()>0) {
			courseLst.setCourseList(extCourseLst);
			courseLst.setState(true);
			courseLst.setMessage("SUCCESS");
			return new ResponseEntity<>(extCourseLst.get(0),HttpStatus.OK);
		}else {
			courseLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(courseLst,HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation(value="Get Course By Creator")
	@PostMapping(value="/getCourseByCreator")
	public ResponseEntity<?> getCourseByCreator(@RequestBody CreatedbyInput createdBy){
		logger.debug("In Get Course by Title");
		CourseLst courseLst = new CourseLst();
		courseLst.setState(false);
		List<Course> extCourseLst = courseService.getCourseByCreator(createdBy.getCreatedBy());
		if(extCourseLst!=null && extCourseLst.size()>0) {
			courseLst.setCourseList(extCourseLst);
			courseLst.setState(true);
			courseLst.setMessage("SUCCESS");
			return new ResponseEntity<>(extCourseLst.get(0),HttpStatus.OK);
		}else {
			courseLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(courseLst,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get All Courses")
	@PostMapping(value="/getAll")
	public ResponseEntity<?> getAll(){
		logger.debug("In Get All Courses");
		List<Course> lstCourse = courseService.getAllCourses();
		CourseLst courseLst = new CourseLst();
		courseLst.setCourseList(lstCourse);
		courseLst.setState(true);
		courseLst.setMessage("SUCCESS");
 		if(lstCourse!=null && lstCourse.size()>0) {
			return new ResponseEntity<>(courseLst,HttpStatus.OK);
		}else {
			courseLst.setState(false);
			courseLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(courseLst,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Course By Exam Category")
	@PostMapping(value="/getCourseByExamCategory")
	public ResponseEntity<?> getCourseByExamCategory(@RequestBody ExamCategoryInput examCategory){
		logger.debug("In Get Course by Exam Category");
		CourseLst courseLst = new CourseLst();
		courseLst.setState(false);
		courseLst.setMessage("FAIL");
		List<Course> extCourseLst = courseService.getCourseByExamCategory(examCategory.getExamCategory());
		if(extCourseLst!=null && extCourseLst.size()>0) {
			courseLst.setCourseList(extCourseLst);
			courseLst.setState(true);
			courseLst.setMessage("SUCCESS");
			return new ResponseEntity<>(extCourseLst,HttpStatus.OK);
		}else {
			courseLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(courseLst,HttpStatus.NOT_FOUND);
		}
	}
	
	public ValidCourse validateCourse(Course course) {
		ValidCourse validCourse = new ValidCourse();
		List<Course> extCourseLst = courseService.getCourseByCourseTitle(course.getCourseTitle());
		if(extCourseLst!=null && extCourseLst.size()>0)
		{
			validCourse.setCourse(extCourseLst.get(0));
			validCourse.setValidationMsg(Constants.ERR_CRS_003);
		}
		return validCourse;
	}
}
