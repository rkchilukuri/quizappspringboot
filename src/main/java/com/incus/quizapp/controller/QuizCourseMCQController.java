/**
 * 
 */
package com.incus.quizapp.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incus.quizapp.model.CourseMCQ;
import com.incus.quizapp.model.CourseMCQLst;
import com.incus.quizapp.model.IdInput;
import com.incus.quizapp.model.TournamentNameInput;
import com.incus.quizapp.service.CourseMCQService;
import com.incus.quizapp.util.Constants;

import io.swagger.annotations.ApiOperation;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 20-Dec-2020
 * Description:
 * *******************************************************
 */
@RestController
@RequestMapping("/quizCourseMCQ")
public class QuizCourseMCQController {
	@Autowired
	CourseMCQService courseMCQService;
	private static final Logger logger = LoggerFactory.getLogger(QuizCourseMCQController.class);
	
	@ApiOperation(value="Add CourseMCQ")
	@PostMapping(value="/add")
	public ResponseEntity<?> addCourseMCQ(@RequestBody CourseMCQ courseMCQ){
		logger.debug("In Add CourseMCQ");
		CourseMCQLst courseMCQLst = new CourseMCQLst();
		courseMCQLst.setState(false);
		if(courseMCQ.getQuestionText()!=null && !courseMCQ.getQuestionText().trim().isEmpty()) {
			courseMCQ.setCreatedDate(LocalDateTime.now());
			courseMCQ.setId(null);
			courseMCQ.setModifiedDate(null);
			courseMCQ.setModifiedBy(null);
			CourseMCQ courseMCQAdded = courseMCQService.addCourseMCQ(courseMCQ);
			List<CourseMCQ> lstCourseMCQ = new ArrayList();
			lstCourseMCQ.add(courseMCQAdded);
			courseMCQLst.setCourseMCQList(lstCourseMCQ);
			courseMCQLst.setState(true);
			courseMCQLst.setMessage("SUCCESS");
			if(courseMCQAdded!=null) {
				return new ResponseEntity<>(courseMCQLst,HttpStatus.OK);
			}else {
				courseMCQLst.setMessage(Constants.ERR_CRS_001);
				return new ResponseEntity<>(courseMCQLst,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			courseMCQLst.setMessage(Constants.ERR_CRS_004);
			return new ResponseEntity<>(courseMCQLst,HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value="Update CourseMCQ")
	@PostMapping(value="/update")
	public ResponseEntity<?> updateCourseMCQ(@RequestBody CourseMCQ courseMCQ){
		logger.debug("In Update CourseMCQ");
		CourseMCQLst courseMCQLst = new CourseMCQLst();
		courseMCQLst.setState(false);
		if(courseMCQ.getQuestionText()!=null && !courseMCQ.getQuestionText().trim().isEmpty()) {
			courseMCQ.setModifiedDate(LocalDateTime.now());
			CourseMCQ courseMCQUpdated = courseMCQService.updateCourseMCQ(courseMCQ);
			List<CourseMCQ> lstCourseMCQ = new ArrayList();
			lstCourseMCQ.add(courseMCQUpdated);
			courseMCQLst.setCourseMCQList(lstCourseMCQ);
			courseMCQLst.setState(true);
			courseMCQLst.setMessage("SUCCESS");
			if(courseMCQUpdated!=null) {
				return new ResponseEntity<>(courseMCQLst,HttpStatus.OK);
			}else {
				courseMCQLst.setMessage(Constants.ERR_CRS_001);
				return new ResponseEntity<>(courseMCQLst,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			courseMCQLst.setMessage(Constants.ERR_CRS_004);
			return new ResponseEntity<>(courseMCQLst,HttpStatus.BAD_REQUEST);
		}
	}
	@ApiOperation(value="Get CourseMCQ")
	@PostMapping(value="/getCourseMCQ")
	public ResponseEntity<?> getCourseMCQ(@RequestBody IdInput courseMCQId){
		logger.debug("In Get CourseMCQ By Id");
		CourseMCQLst courseMCQLst = new CourseMCQLst();
		courseMCQLst.setState(false);
		CourseMCQ extCourseMCQ = courseMCQService.getCourseMCQById(courseMCQId.getId());
		if(extCourseMCQ!=null) {
			List<CourseMCQ> lstCourseMCQ = new ArrayList();
			lstCourseMCQ.add(extCourseMCQ);
			courseMCQLst.setCourseMCQList(lstCourseMCQ);
			courseMCQLst.setState(true);
			courseMCQLst.setMessage("SUCCESS");
			return new ResponseEntity<>(courseMCQLst,HttpStatus.OK);
		}else {
			courseMCQLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(courseMCQLst,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get CourseMCQ By Tournament")
	@PostMapping(value="/getCourseMCQByTournament")
	public ResponseEntity<?> getCourseMCQByTournament(@RequestBody TournamentNameInput tournamentName){
		logger.debug("In Get CourseMCQ by Name");
		CourseMCQLst courseMCQLst = new CourseMCQLst();
		courseMCQLst.setState(false);
		List<CourseMCQ> extCourseMCQLst = courseMCQService.getCourseMCQByTournament(tournamentName.getTournamentName());
		if(extCourseMCQLst!=null && extCourseMCQLst.size()>0) {
			courseMCQLst.setCourseMCQList(extCourseMCQLst);
			courseMCQLst.setState(true);
			courseMCQLst.setMessage("SUCCESS");
			return new ResponseEntity<>(courseMCQLst,HttpStatus.OK);
		}else {
			courseMCQLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(courseMCQLst,HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation(value="Get All CourseMCQs")
	@PostMapping(value="/getAll")
	public ResponseEntity<?> getAll(){
		logger.debug("In Get All CourseMCQs");
		List<CourseMCQ> lstCourseMCQ = courseMCQService.getAllCourseMCQs();
		CourseMCQLst courseMCQLst = new CourseMCQLst();
		courseMCQLst.setCourseMCQList(lstCourseMCQ);
		courseMCQLst.setState(true);
		courseMCQLst.setMessage("SUCCESS");
 		if(lstCourseMCQ!=null) {
			return new ResponseEntity<>(courseMCQLst,HttpStatus.OK);
		}else {
			courseMCQLst.setState(false);
			courseMCQLst.setMessage(Constants.ERR_CRS_002);
			return new ResponseEntity<>(courseMCQLst,HttpStatus.NOT_FOUND);
		}
	}
}
