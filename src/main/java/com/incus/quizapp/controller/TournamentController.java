/**
 * 
 */
package com.incus.quizapp.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incus.quizapp.model.IdInput;
import com.incus.quizapp.model.TitleInput;
import com.incus.quizapp.model.Tournament;
import com.incus.quizapp.model.TournamentInput;
import com.incus.quizapp.model.TournamentLst;
import com.incus.quizapp.service.TournamentService;
import com.incus.quizapp.util.Constants;

import io.swagger.annotations.ApiOperation;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
@RestController
@RequestMapping("/tournament")
public class TournamentController {
	@Autowired
	TournamentService tournamentService;
	private static final Logger logger = LoggerFactory.getLogger(TournamentController.class);
	
	@ApiOperation(value="Add Tournament")
	@PostMapping(value="/add")
	public ResponseEntity<?> addTournament(@RequestBody Tournament tournament){
		logger.debug("In Add Tournament");
		if(tournament.getTournamentTitle()!=null && !tournament.getTournamentTitle().trim().isEmpty()) {
			tournament.setCreatedDate(LocalDateTime.now());
			tournament.setId(null);
			tournament.setModifiedDate(null);
			tournament.setModifiedBy(null);
			Tournament tournamentAdded = tournamentService.addTournament(tournament);
			List<Tournament> lstTournament = new ArrayList();
			lstTournament.add(tournamentAdded);
			TournamentLst tournamentLst = new TournamentLst();
			tournamentLst.setTournamentList(lstTournament);
			tournamentLst.setState(true);
			tournamentLst.setMessage("SUCCESS");
			if(tournamentAdded!=null) {
				return new ResponseEntity<>(tournamentLst,HttpStatus.OK);
			}else {
				return new ResponseEntity<>(Constants.ERR_CRS_001,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(Constants.ERR_CRS_004,HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value="Update Tournament")
	@PostMapping(value="/update")
	public ResponseEntity<?> updateTournament(@RequestBody Tournament tournament){
		logger.debug("In Update Tournament");
		if(tournament.getTournamentTitle()!=null && !tournament.getTournamentTitle().trim().isEmpty()) {
			tournament.setModifiedDate(LocalDateTime.now());
			Tournament tournamentUpdated = tournamentService.updateTournament(tournament);
			List<Tournament> lstTournament = new ArrayList();
			lstTournament.add(tournamentUpdated);
			TournamentLst tournamentLst = new TournamentLst();
			tournamentLst.setTournamentList(lstTournament);
			tournamentLst.setState(true);
			tournamentLst.setMessage("SUCCESS");
			if(tournamentUpdated!=null) {
				return new ResponseEntity<>(tournamentLst,HttpStatus.OK);
			}else {
				return new ResponseEntity<>(Constants.ERR_CRS_001,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(Constants.ERR_CRS_004,HttpStatus.BAD_REQUEST);
		}
	}
	@ApiOperation(value="Get Tournament")
	@PostMapping(value="/getTournament")
	public ResponseEntity<?> getTournament(@RequestBody IdInput tournamentId){
		logger.debug("In Get Tournament By Id");
		Tournament extTournament = tournamentService.getTournament(tournamentId.getId());
		if(extTournament!=null) {
			return new ResponseEntity<>(extTournament,HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_CRS_002,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Tournament By Title")
	@PostMapping(value="/getTournamentByTitle")
	public ResponseEntity<?> getTournamentByTitle(@RequestBody TitleInput tournamentTitle){
		logger.debug("In Get Tournament by Title");
		List<Tournament> extTournamentLst = tournamentService.getTournamentByTournamentTitle(tournamentTitle.getTitle());
		if(extTournamentLst!=null&&extTournamentLst.size()>0) {
			return new ResponseEntity<>(extTournamentLst.get(0),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_CRS_002,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Tournament By Category Course and Chapter")
	@PostMapping(value="/getTournamentByCategory")
	public ResponseEntity<?> getTournamentByCategory(@RequestBody TournamentInput tournamentInput){
		logger.debug("In Get Tournament By Category Course and Chapter");
		List<Tournament> extTournamentLst = tournamentService.getTournamentByCategoryCourseChapter(tournamentInput.getExamCategory(),
				tournamentInput.getCourse(),tournamentInput.getChapter());
		if(extTournamentLst!=null&&extTournamentLst.size()>0) {
			return new ResponseEntity<>(extTournamentLst.get(0),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_CRS_002,HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation(value="Get All Tournaments")
	@PostMapping(value="/getAll")
	public ResponseEntity<?> getAll(){
		logger.debug("In Get All Tournaments");
		List<Tournament> lstTournament = tournamentService.getAllTournaments();
		TournamentLst tournamentLst = new TournamentLst();
		tournamentLst.setTournamentList(lstTournament);
		tournamentLst.setState(true);
		tournamentLst.setMessage("SUCCESS");
 		if(lstTournament!=null) {
			return new ResponseEntity<>(tournamentLst,HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_CRS_002,HttpStatus.NOT_FOUND);
		}
	}
	
}
