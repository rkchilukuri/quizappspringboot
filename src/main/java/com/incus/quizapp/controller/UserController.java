package com.incus.quizapp.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incus.quizapp.model.IdInput;
import com.incus.quizapp.model.User;
import com.incus.quizapp.model.UserInput;
import com.incus.quizapp.model.UserLst;
import com.incus.quizapp.service.UserService;
import com.incus.quizapp.util.Constants;

import io.swagger.annotations.ApiOperation;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 18-Dec-2020
 * Description:
 * *******************************************************
 */
@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserService userService;
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@ApiOperation(value="Add User")
	@PostMapping(value="/INCUSUserRegistration")
	public ResponseEntity<?> addUser(@RequestBody User user){
		logger.debug("In Add User");
		if(user.getName()!=null && !user.getName().trim().isEmpty()) {
			user.setCreatedDate(LocalDateTime.now());
			user.setId(null);
			user.setModifiedDate(null);
			user.setModifiedBy(null);
			User userAdded = userService.addUser(user);
			List<User> lstUser = new ArrayList();
			lstUser.add(userAdded);
			UserLst userLst = new UserLst();
			userLst.setUserList(lstUser);
			userLst.setState(true);
			userLst.setMessage("SUCCESS");
			if(userAdded!=null) {
				return new ResponseEntity<>(userLst,HttpStatus.OK);
			}else {
				return new ResponseEntity<>(Constants.ERR_USR_001,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(Constants.ERR_USR_004,HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value="Verify User")
	@PostMapping(value="/VerifyUser")
	public ResponseEntity<?> verifyUser(@RequestBody UserInput userInput){
		User user = null;
		List<User> users = new ArrayList();
		UserLst userLst = new UserLst();
		if(userInput.getEmailId()!=null) {
			user = userService.getUserByEmail(userInput.getEmailId());
			if(user!=null) {
				users.add(user);
			}
		}else if(userInput.getPhoneNo()!=null) {
			user = userService.getUserByPhoneNo(userInput.getPhoneNo());
			if(user!=null) {
				users.add(user);
			}
		}else if(userInput.getUserName()!=null && userInput.getPassword()!=null) {
			users = userService.getUserByUserNameAndPassword(userInput.getUserName(),userInput.getPassword());
		}
		if(users!=null && users.size()>0) {
			userLst.setUserList(users);
			userLst.setState(true);
			userLst.setMessage("SUCCESS");
			return new ResponseEntity<>(userLst,HttpStatus.OK);
		}else {
			userLst.setState(false);
			userLst.setMessage("FAIL");
			return new ResponseEntity<>(userLst,HttpStatus.NO_CONTENT);
		}
	}
	
	@ApiOperation(value="Update User")
	@PostMapping(value="/update")
	public ResponseEntity<?> updateUser(@RequestBody User user){
		logger.debug("In Update User");
		if(user.getName()!=null && !user.getName().trim().isEmpty()) {
			user.setModifiedDate(LocalDateTime.now());
			User userUpdated = userService.updateUser(user);
			List<User> lstUser = new ArrayList();
			lstUser.add(userUpdated);
			UserLst userLst = new UserLst();
			userLst.setUserList(lstUser);
			userLst.setState(true);
			userLst.setMessage("SUCCESS");
			if(userUpdated!=null) {
				return new ResponseEntity<>(userLst,HttpStatus.OK);
			}else {
				return new ResponseEntity<>(Constants.ERR_USR_001,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(Constants.ERR_USR_004,HttpStatus.BAD_REQUEST);
		}
	}
	@ApiOperation(value="Get User By Id")
	@PostMapping(value="/getUser")
	public ResponseEntity<?> getUser(@RequestBody IdInput userId){
		logger.debug("In Get User By Id");
		User extUser = userService.getUser(userId.getId());
		if(extUser!=null) {
			return new ResponseEntity<>(extUser,HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_USR_002,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get User By Name")
	@PostMapping(value="/getUserByName")
	public ResponseEntity<?> getUserByName(@RequestBody String userName){
		logger.debug("In Get User by Name");
		List<User> extUserLst = userService.getUserByUserName(userName);
		if(extUserLst!=null&&extUserLst.size()>0) {
			return new ResponseEntity<>(extUserLst.get(0),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_USR_002,HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation(value="Get All Users")
	@PostMapping(value="/getAll")
	public ResponseEntity<?> getAll(){
		logger.debug("In Get All Users");
		List<User> lstUser = userService.getAllUsers();
		UserLst userLst = new UserLst();
		userLst.setUserList(lstUser);
		userLst.setState(true);
		userLst.setMessage("SUCCESS");
 		if(lstUser!=null) {
			return new ResponseEntity<>(userLst,HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_USR_002,HttpStatus.NOT_FOUND);
		}
	}
}
