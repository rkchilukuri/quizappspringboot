/**
 * 
 */
package com.incus.quizapp.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.incus.quizapp.model.Chapter;
import com.incus.quizapp.model.ChapterLst;
import com.incus.quizapp.model.CourseInput;
import com.incus.quizapp.model.IdInput;
import com.incus.quizapp.model.TitleInput;
import com.incus.quizapp.model.VideoChapter;
import com.incus.quizapp.model.VideoChapterLst;
import com.incus.quizapp.service.VideoChapterService;
import com.incus.quizapp.util.Constants;

import io.swagger.annotations.ApiOperation;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
@RestController
@RequestMapping("/videoChapter")
public class VideoChapterController {
	@Autowired
	VideoChapterService chapterService;
	
	private static final Logger logger = LoggerFactory.getLogger(VideoChapterController.class);
	
	@ApiOperation(value="Add Video Chapter")
	@PostMapping(value="/add")
	public ResponseEntity<?> addVideoChapter(@RequestBody VideoChapter chapter){
		logger.debug("In Add Chapter");
		if(chapter.getChapterTitle()!=null && !chapter.getChapterTitle().trim().isEmpty()) {
			chapter.setCreatedDate(LocalDateTime.now());
			chapter.setId(null);
			chapter.setModifiedDate(null);
			chapter.setModifiedBy(null);
			VideoChapter chapterAdded = chapterService.addChapter(chapter);
			List<VideoChapter> lstChapter = new ArrayList();
			lstChapter.add(chapterAdded);
			VideoChapterLst chapterLst = new VideoChapterLst();
			chapterLst.setChapterList(lstChapter);
			chapterLst.setState(true);
			chapterLst.setMessage("SUCCESS");
			if(chapterAdded!=null) {
				return new ResponseEntity<>(chapterLst,HttpStatus.OK);
			}else {
				return new ResponseEntity<>(Constants.ERR_CPT_001,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(Constants.ERR_CPT_004,HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value="Update Chapter")
	@PostMapping(value="/update")
	public ResponseEntity<?> updateChapter(@RequestBody VideoChapter chapter){
		logger.debug("In Update Chapter");
		if(chapter.getChapterTitle()!=null && !chapter.getChapterTitle().trim().isEmpty()) {
			chapter.setModifiedDate(LocalDateTime.now());
			VideoChapter chapterUpdated = chapterService.updateChapter(chapter);
			List<VideoChapter> lstChapter = new ArrayList();
			lstChapter.add(chapterUpdated);
			VideoChapterLst chapterLst = new VideoChapterLst();
			chapterLst.setChapterList(lstChapter);
			chapterLst.setState(true);
			chapterLst.setMessage("SUCCESS");
			if(chapterUpdated!=null) {
				return new ResponseEntity<>(chapterLst,HttpStatus.OK);
			}else {
				return new ResponseEntity<>(Constants.ERR_CPT_001,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}else {
			return new ResponseEntity<>(Constants.ERR_CPT_004,HttpStatus.BAD_REQUEST);
		}
	}
	
	@ApiOperation(value="Get Chapter")
	@PostMapping(value="/getChapter")
	public ResponseEntity<?> getChapter(@RequestBody IdInput chapterId){
		logger.debug("In Get Chapter By Id");
		VideoChapter extChapter = chapterService.getChapter(chapterId.getId());
		if(extChapter!=null) {
			return new ResponseEntity<>(extChapter,HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_CPT_002,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Chapter By Title")
	@PostMapping(value="/getChapterByTitle")
	public ResponseEntity<?> getChapterByTitle(@RequestBody TitleInput chapterTitle){
		logger.debug("In Get Chapter by Title");
		List<VideoChapter> extChapterLst = chapterService.getChapterByChapterTitle(chapterTitle.getTitle());
		if(extChapterLst!=null && extChapterLst.size()>0) {
			return new ResponseEntity<>(extChapterLst.get(0),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_CPT_002,HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation(value="Get All Chapters")
	@PostMapping(value="/getAll")
	public ResponseEntity<?> getAll(){
		logger.debug("In Get All Chapters");
		List<VideoChapter> lstChapter = chapterService.getAllChapters();
		VideoChapterLst chapterLst = new VideoChapterLst();
		chapterLst.setChapterList(lstChapter);
		chapterLst.setState(true);
		chapterLst.setMessage("SUCCESS");
 		if(lstChapter!=null) {
			return new ResponseEntity<>(chapterLst,HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_CPT_002,HttpStatus.NOT_FOUND);
		}
	}
	
	@ApiOperation(value="Get Chapter By Video Course")
	@PostMapping(value="/getChapterByCourse")
	public ResponseEntity<?> getChapterByCourse(@RequestBody CourseInput videoCourseName){
		logger.debug("In Get Chapter by Video Course");
		List<VideoChapter> extChapterLst = chapterService.getChapterByCourse(videoCourseName.getCourse());
		if(extChapterLst!=null && extChapterLst.size()>0) {
			return new ResponseEntity<>(extChapterLst.get(0),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(Constants.ERR_CPT_002,HttpStatus.NOT_FOUND);
		}
	}
	
}
