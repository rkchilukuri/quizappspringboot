/**
 * 
 */
package com.incus.quizapp.model;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 20-Dec-2020
 * Description:
 * *******************************************************
 */
@Getter
@Setter
@ToString
public class CourseMCQ {
	@Id
	private String id;
	private Integer timeLimit;
	private String examCategory;
	private String course;
	private String chapter;
	private String tournament;
	private String subject;
	private String topic;
	private String explanation;
	private String image;
	private String video;
	private String audio;
	private String questionText;
	private String optionAText;
	private String optionAImage;
	private String optionBText;
	private String optionBImage;
	private String optionCText;
	private String optionCImage;
	private String optionDText;
	private String optionDImage;
	private String optionEText;
	private String optionEImage;
	private List<String> explanationImages;
	private String correctAnswer;
	private String createdBy;
	private LocalDateTime createdDate;
	private String modifiedBy;
	private LocalDateTime modifiedDate;
}
