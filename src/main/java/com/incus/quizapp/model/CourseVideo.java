/**
 * 
 */
package com.incus.quizapp.model;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 20-Dec-2020
 * Description:
 * *******************************************************
 */
@Getter
@Setter
@ToString
public class CourseVideo {
	@Id
	private String id;
	private String examCategory;
	private String course;
	private String chapter;
	private String title;
	private String description;
	private String mainImage;
	private String videoLink;
	private String vimeoVideoLink;
	private String powerPointLink;
	private String powerPointLinkFromDrive;
	private String notesImageLink;
	private List<String> imageLinks;
	private Boolean published;
	private Boolean preview;
	private String createdBy;
	private LocalDateTime createdDate;
	private String modifiedBy;
	private LocalDateTime modifiedDate;
}
