/**
 * 
 */
package com.incus.quizapp.model;

import lombok.Getter;
import lombok.Setter;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 27-Dec-2020
 * Description:
 * *******************************************************
 */
@Getter
@Setter
public class CreatedbyInput {
	private String createdBy;
}
