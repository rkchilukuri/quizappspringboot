/**
 * 
 */
package com.incus.quizapp.model;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
@Getter
@Setter
@ToString
public class Deck {
	@Id
	private String id;
	private String flashcardCourse;
	private String deckTitle;
	private String coverImage;
	private String description;
	private Boolean published;
	private Boolean preview;
	private String createdBy;
	private LocalDateTime createdDate;
	private String modifiedBy;
	private LocalDateTime modifiedDate;
}
