/**
 * 
 */
package com.incus.quizapp.model;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 19-Dec-2020
 * Description:
 * *******************************************************
 */
@Getter
@Setter
@ToString
public class ExamCategory {
	@Id
	private String id;
	private String categoryName;
	private String country;
	private String description;
	private String coverImage;
	private String createdBy;
	private LocalDateTime createdDate;
	private String modifiedBy;
	private LocalDateTime modifiedDate;
}
