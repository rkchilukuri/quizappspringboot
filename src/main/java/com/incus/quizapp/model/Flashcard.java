/**
 * 
 */
package com.incus.quizapp.model;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 22-Dec-2020
 * Description:
 * *******************************************************
 */
@Getter
@Setter
@ToString
public class Flashcard {
	@Id
	private String id;
	private String course;
	private String deck;
	private String title;
	private String frontText;
	private String frontImageFile;
	private String frontVoiceFile;
	private String backText;
	private String backImageFile;
	private String backVoiceFile;
	private String createdBy;
	private LocalDateTime createdDate;
	private String modifiedBy;
	private LocalDateTime modifiedDate;	
}
