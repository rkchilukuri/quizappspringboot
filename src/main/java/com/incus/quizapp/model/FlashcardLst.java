/**
 * 
 */
package com.incus.quizapp.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 22-Dec-2020
 * Description:
 * *******************************************************
 */
@Getter
@Setter
public class FlashcardLst {
	private List<Flashcard> flashcardList;
	private Boolean state;
	private String message;
}
