/**
 * 
 */
package com.incus.quizapp.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
@Getter
@Setter
public class LiveclassChapterLst {
	List<LiveclassChapter> liveclassChapterList;
	private Boolean state;
	private  String message;
}
