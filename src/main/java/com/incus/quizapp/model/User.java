/**
 * 
 */
package com.incus.quizapp.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 18-Dec-2020
 * Description:
 * *******************************************************
 */
@Getter
@Setter
@ToString
public class User {
	@Id
	private String id;
	private String name;
	private String userAlias;
	private String phoneNo;
	private String emailId;
	private String password;
	private String registerFrom;
	private String city;
	private LocalDate dob;
	private String gcmId;
	private String profilePic;
	private String createdBy;
	private LocalDateTime createdDate;
	private String modifiedBy;
	private LocalDateTime modifiedDate;
	private LocalDateTime expiryDate;
	private String role;//LCC/Admin/Student
	private Integer gamesPlayed;
	private LocalDateTime lastLogin;
}
