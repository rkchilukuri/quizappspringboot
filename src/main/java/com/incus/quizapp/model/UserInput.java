/**
 * 
 */
package com.incus.quizapp.model;

import lombok.Getter;
import lombok.Setter;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 19-Dec-2020
 * Description:
 * *******************************************************
 */
@Getter
@Setter
public class UserInput {
	private String userName;
	private String password;
	private String phoneNo;
	private String emailId;
}
