package com.incus.quizapp.model;

import lombok.Getter;
import lombok.Setter;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 05-Sep-2020
 * Description:
 * *******************************************************
 */
@Getter
@Setter
public class ValidCourse {
	Course course;
	String validationMsg;
}
