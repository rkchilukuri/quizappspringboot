/**
 * 
 */
package com.incus.quizapp.model;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
@Getter
@Setter
@ToString
public class VideoCourse {
	@Id
	private String id;
	private String videoTitle;
	private String examCategory;
	private String coverImage;
	private String aboutCourse;
	private String authorName;
	private String aboutAuthor;
	private Integer totalNumberOfMCQsOffered;
	private Long priceOfTheCourse;
	private Integer subscriptionDuration;
	private Integer totalNumberOfTournaments;
	private Long videoCoursePin;
	private Boolean published;
	private Boolean forPublic;
	private String createdBy;
	private LocalDateTime createdDate;
	private String modifiedBy;
	private LocalDateTime modifiedDate;
}
