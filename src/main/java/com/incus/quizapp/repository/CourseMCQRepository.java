/**
 * 
 */
package com.incus.quizapp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.incus.quizapp.model.CourseMCQ;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 20-Dec-2020
 * Description:
 * *******************************************************
 */
public interface CourseMCQRepository extends MongoRepository<CourseMCQ, String> {
	List<CourseMCQ> findByTournament(String tournament);
}
