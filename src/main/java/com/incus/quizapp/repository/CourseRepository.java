/**
 * 
 */
package com.incus.quizapp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.incus.quizapp.model.Course;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
public interface CourseRepository extends MongoRepository<Course, String> {
	List<Course> findByCourseTitle(String courseTitle);
	List<Course> findByExamCategory(String examCategory);
	List<Course> findByCreatedBy(String createdBy);
}
