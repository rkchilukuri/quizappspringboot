/**
 * 
 */
package com.incus.quizapp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.incus.quizapp.model.CourseVideo;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 21-Dec-2020
 * Description:
 * *******************************************************
 */
public interface CourseVideoRepository extends MongoRepository<CourseVideo, String> {
	List<CourseVideo> findByChapter(String chapter);
	List<CourseVideo> findByExamCategoryAndCourseAndChapter(String examCategory,String Course,String chapter);
}
