/**
 * 
 */
package com.incus.quizapp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.incus.quizapp.model.Deck;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
public interface DeckRepository extends MongoRepository<Deck, String> {
	List<Deck> findByDeckTitle(String deckTitle);
	List<Deck> findByFlashcardCourse(String flashcardCourse);
}
