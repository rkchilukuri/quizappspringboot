/**
 * 
 */
package com.incus.quizapp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.incus.quizapp.model.ExamCategory;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 20-Dec-2020
 * Description:
 * *******************************************************
 */
public interface ExamCategoryRepository extends MongoRepository<ExamCategory, String> {
	List<ExamCategory> findByCategoryName(String categoryName);
	List<ExamCategory> findByCountry(String country);

}
