/**
 * 
 */
package com.incus.quizapp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.incus.quizapp.model.Course;
import com.incus.quizapp.model.FlashcardCourse;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
public interface FlashcardCourseRepository  extends MongoRepository<FlashcardCourse, String>{
	List<FlashcardCourse> findByFlashcardTitle(String flashcardTitle);
	List<FlashcardCourse> findByExamCategory(String examCategory);
	List<Course> findByCreatedBy(String createdBy);
}
