/**
 * 
 */
package com.incus.quizapp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.incus.quizapp.model.Flashcard;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 22-Dec-2020
 * Description:
 * *******************************************************
 */
public interface FlashcardRepository extends MongoRepository<Flashcard, String>{
	List<Flashcard> findByTitle(String title);
	List<Flashcard> findByDeck(String deck);
}
