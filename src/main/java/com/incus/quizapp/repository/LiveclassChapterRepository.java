/**
 * 
 */
package com.incus.quizapp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.incus.quizapp.model.LiveclassChapter;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
public interface LiveclassChapterRepository  extends MongoRepository<LiveclassChapter, String> {
	List<LiveclassChapter> findByLiveclassChapterTitle(String liveclassChapterTitle);
	List<LiveclassChapter> findByCourse(String course);
}
