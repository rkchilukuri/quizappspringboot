/**
 * 
 */
package com.incus.quizapp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.incus.quizapp.model.Course;
import com.incus.quizapp.model.LiveclassCourse;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
public interface LiveclassCourseRepository extends MongoRepository<LiveclassCourse, String>{
	List<LiveclassCourse> findByLiveclassTitle(String liveclassTitle);
	List<LiveclassCourse> findByExamCategory(String examCategory);
	List<Course> findByCreatedBy(String createdBy);
}
