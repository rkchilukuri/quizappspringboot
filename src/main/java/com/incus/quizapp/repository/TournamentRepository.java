package com.incus.quizapp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.incus.quizapp.model.Tournament;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
public interface TournamentRepository extends MongoRepository<Tournament, String> {
	List<Tournament> findByTournamentTitle(String tournamentTitle);
	List<Tournament> findByExamCategoryAndCourseAndChapter(String examCategory,String course, String chapter);
}
