/**
 * 
 */
package com.incus.quizapp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.incus.quizapp.model.User;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 19-Dec-2020
 * Description:
 * *******************************************************
 */
public interface UserRepository  extends MongoRepository<User, String> {
	List<User> findByName(String userName);
	List<User> findByPhoneNo(String phoneNo);
	List<User> findByEmailId(String emailId);
	List<User> findByNameAndPassword(String userName, String password);
}
