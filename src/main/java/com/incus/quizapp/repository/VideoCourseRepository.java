/**
 * 
 */
package com.incus.quizapp.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.incus.quizapp.model.Course;
import com.incus.quizapp.model.VideoCourse;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
public interface VideoCourseRepository extends MongoRepository<VideoCourse, String>{
	List<VideoCourse> findByVideoTitle(String videoTitle);
	List<VideoCourse> findByExamCategory(String examCategory);
	List<Course> findByCreatedBy(String createdBy);
}
