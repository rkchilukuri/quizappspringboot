/**
 * 
 */
package com.incus.quizapp.service;

import java.util.List;

import com.incus.quizapp.model.Chapter;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
public interface ChapterService {
	List<Chapter> getChapterByChapterTitle(String chapterTitle);
	Chapter addChapter(Chapter chapter);
	Chapter getChapter(String chapterId);
	List<Chapter> getAllChapters();
	Chapter updateChapter(Chapter chapter);
	List<Chapter> getChapterByCourse(String course);
}
