/**
 * 
 */
package com.incus.quizapp.service;

import java.util.List;

import com.incus.quizapp.model.CourseMCQ;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 20-Dec-2020
 * Description:
 * *******************************************************
 */
public interface CourseMCQService {
	CourseMCQ addCourseMCQ(CourseMCQ courseMCQ);
	CourseMCQ updateCourseMCQ(CourseMCQ courseMCQ);
	CourseMCQ getCourseMCQById(String courseMCQId);
	List<CourseMCQ> getCourseMCQByTournament(String tournamentName);
	List<CourseMCQ> getAllCourseMCQs();
}
