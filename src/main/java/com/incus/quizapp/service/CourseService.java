/**
 * 
 */
package com.incus.quizapp.service;

import java.util.List;

import com.incus.quizapp.model.Course;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
public interface CourseService {
	List<Course> getCourseByCourseTitle(String courseTitle);
	Course addCourse(Course course);
	Course getCourse(String courseId);
	List<Course> getAllCourses();
	Course updateCourse(Course course);
	List<Course> getCourseByExamCategory(String examCategory);
	List<Course> getCourseByCreator(String createdBy);
}
