/**
 * 
 */
package com.incus.quizapp.service;

import java.util.List;

import com.incus.quizapp.model.CourseVideo;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 21-Dec-2020
 * Description:
 * *******************************************************
 */
public interface CourseVideoService {
	CourseVideo addCourseVideo(CourseVideo courseVideo);
	CourseVideo updateCourseVideo(CourseVideo courseVideo);
	CourseVideo getCourseVideoById(String courseVideoId);
	List<CourseVideo> getCourseVideoByChapter(String chapterName);
	List<CourseVideo> getAllCourseVideos();
	List<CourseVideo> getCourseVideoByExamCategoryCourseChapter(String examCategory, String course, String chapter);
}
