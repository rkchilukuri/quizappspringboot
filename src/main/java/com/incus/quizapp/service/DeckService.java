/**
 * 
 */
package com.incus.quizapp.service;

import java.util.List;

import com.incus.quizapp.model.Deck;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
public interface DeckService {
	List<Deck> getDeckByDeckTitle(String deckTitle);
	Deck addDeck(Deck deck);
	Deck getDeck(String deckId);
	List<Deck> getAllDecks();
	Deck updateDeck(Deck deck);
	List<Deck> getByFlashcardCourse(String flashcardCourse);
}
