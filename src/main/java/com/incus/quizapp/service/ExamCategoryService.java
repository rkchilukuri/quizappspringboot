/**
 * 
 */
package com.incus.quizapp.service;

import java.util.List;

import com.incus.quizapp.model.ExamCategory;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 19-Dec-2020
 * Description:
 * *******************************************************
 */
public interface ExamCategoryService {
	ExamCategory addExamCategory(ExamCategory examCategory);
	ExamCategory updateExamCategory(ExamCategory examCategory);
	List<ExamCategory> getAllExamCategorys();
	List<ExamCategory> getExamCategoryByCategoryName(String examCategoryName);
	ExamCategory getExamCategoryById(String examCategoryId);
	List<ExamCategory> getExamCategoryByCountry(String country);
}
