/**
 * 
 */
package com.incus.quizapp.service;

import java.util.List;

import com.incus.quizapp.model.Course;
import com.incus.quizapp.model.FlashcardCourse;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
public interface FlashcardCourseService {
	List<FlashcardCourse> getCourseByCourseTitle(String courseTitle);
	FlashcardCourse addCourse(FlashcardCourse course);
	FlashcardCourse getCourse(String courseId);
	List<FlashcardCourse> getAllCourses();
	FlashcardCourse updateCourse(FlashcardCourse course);
	List<FlashcardCourse> getCourseByExamCategory(String examCategory);
	List<Course> getCourseByCreator(String createdBy);
}
