/**
 * 
 */
package com.incus.quizapp.service;

import java.util.List;

import com.incus.quizapp.model.Flashcard;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 22-Dec-2020
 * Description:
 * *******************************************************
 */
public interface FlashcardService {

	Flashcard addFlashcard(Flashcard flashcard);
	Flashcard updateFlashcard(Flashcard flashcard);
	Flashcard getFlashcard(String flashcardId);
	List<Flashcard> getFlashcardByFlashcardTitle(String flashcardTitle);
	List<Flashcard> getAllFlashcards();
	List<Flashcard> getFlashcardByDeck(String deck);
}
