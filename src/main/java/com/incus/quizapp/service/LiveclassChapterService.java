/**
 * 
 */
package com.incus.quizapp.service;

import java.util.List;

import com.incus.quizapp.model.LiveclassChapter;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
public interface LiveclassChapterService {
	List<LiveclassChapter> getLiveclassChapterByLiveclassChapterTitle(String chapterTitle);
	LiveclassChapter addLiveclassChapter(LiveclassChapter chapter);
	LiveclassChapter getLiveclassChapter(String chapterId);
	List<LiveclassChapter> getAllLiveclassChapters();
	LiveclassChapter updateLiveclassChapter(LiveclassChapter chapter);
	List<LiveclassChapter> getLiveclassChapterByCourse(String course);
}
