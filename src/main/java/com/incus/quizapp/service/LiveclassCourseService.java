/**
 * 
 */
package com.incus.quizapp.service;

import java.util.List;

import com.incus.quizapp.model.Course;
import com.incus.quizapp.model.LiveclassCourse;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
public interface LiveclassCourseService {
	List<LiveclassCourse> getCourseByCourseTitle(String courseTitle);
	LiveclassCourse addCourse(LiveclassCourse course);
	LiveclassCourse getCourse(String courseId);
	List<LiveclassCourse> getAllCourses();
	LiveclassCourse updateCourse(LiveclassCourse course);
	List<LiveclassCourse> getCourseByExamCategory(String examCategory);
	List<Course> getCourseByCreator(String createdBy);
}
