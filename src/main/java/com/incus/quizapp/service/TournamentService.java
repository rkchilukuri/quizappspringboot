/**
 * 
 */
package com.incus.quizapp.service;

import java.util.List;

import com.incus.quizapp.model.Tournament;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
public interface TournamentService {
	List<Tournament> getTournamentByTournamentTitle(String tournamentTitle);
	Tournament addTournament(Tournament tournament);
	Tournament getTournament(String tournamentId);
	List<Tournament> getAllTournaments();
	Tournament updateTournament(Tournament tournament);
	List<Tournament> getTournamentByCategoryCourseChapter(String examCategory, String course, String chapter);
}
