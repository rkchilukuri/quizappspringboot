/**
 * 
 */
package com.incus.quizapp.service;

import java.util.List;

import com.incus.quizapp.model.User;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 19-Dec-2020
 * Description:
 * *******************************************************
 */
public interface UserService {
	List<User> getUserByUserName(String userName);
	User updateUser(User user);
	User addUser(User user);
	List<User> getAllUsers();
	User getUser(String userId);
	User getUserByPhoneNo(String phoneNo);
	User getUserByEmail(String email);
	List<User> getUserByUserNameAndPassword(String userName, String password);
}
