/**
 * 
 */
package com.incus.quizapp.service;

import java.util.List;

import com.incus.quizapp.model.VideoChapter;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
public interface VideoChapterService {
	VideoChapter addChapter(VideoChapter chapter);
	List<VideoChapter> getChapterByChapterTitle(String chapterTitle);
	VideoChapter getChapter(String chapterId);
	List<VideoChapter> getAllChapters();
	VideoChapter updateChapter(VideoChapter chapter);
	List<VideoChapter> getChapterByCourse(String course);
}
