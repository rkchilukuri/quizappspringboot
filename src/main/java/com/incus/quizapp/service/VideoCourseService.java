/**
 * 
 */
package com.incus.quizapp.service;

import java.util.List;

import com.incus.quizapp.model.Course;
import com.incus.quizapp.model.VideoCourse;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
public interface VideoCourseService {
	List<VideoCourse> getCourseByCourseTitle(String courseTitle);
	VideoCourse addCourse(VideoCourse course);
	VideoCourse getCourse(String courseId);
	List<VideoCourse> getAllCourses();
	VideoCourse updateCourse(VideoCourse course);
	List<VideoCourse> getCourseByExamCategory(String examCategory);
	List<Course> getCourseByCreator(String createdBy);
}
