/**
 * 
 */
package com.incus.quizapp.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incus.quizapp.model.Course;
import com.incus.quizapp.model.CourseMCQ;
import com.incus.quizapp.repository.CourseMCQRepository;
import com.incus.quizapp.service.CourseMCQService;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 20-Dec-2020
 * Description:
 * *******************************************************
 */
@Service
public class CourseMCQServiceImpl implements CourseMCQService{
	@Autowired
	CourseMCQRepository courseMCQRepo;
	
	@Override
	public CourseMCQ addCourseMCQ(CourseMCQ courseMCQ) {
		return courseMCQRepo.save(courseMCQ);
	}

	@Override
	public CourseMCQ updateCourseMCQ(CourseMCQ courseMCQ) {
		if(courseMCQ.getId()!=null) {
			CourseMCQ extCourseMCQ = courseMCQRepo.findById(courseMCQ.getId()).orElse(null);
			if(extCourseMCQ!=null) {
				if(courseMCQ.getExamCategory()!=null) {
					extCourseMCQ.setExamCategory(courseMCQ.getExamCategory());
				}
				if(courseMCQ.getCourse()!=null) {
					extCourseMCQ.setCourse(courseMCQ.getCourse());
				}
				if(courseMCQ.getChapter()!=null) {
					extCourseMCQ.setChapter(courseMCQ.getChapter());
				}
				if(courseMCQ.getTournament()!=null) {
					extCourseMCQ.setTournament(courseMCQ.getTournament());
				}
				if(courseMCQ.getSubject()!=null) {
					extCourseMCQ.setSubject(courseMCQ.getSubject());
				}
				if(courseMCQ.getTopic()!=null) {
					extCourseMCQ.setTopic(courseMCQ.getTopic());
				}
				if(courseMCQ.getExplanation()!=null) {
					extCourseMCQ.setExplanation(courseMCQ.getExplanation());
				}
				if(courseMCQ.getImage()!=null) {
					extCourseMCQ.setImage(courseMCQ.getImage());
				}
				if(courseMCQ.getVideo()!=null) {
					extCourseMCQ.setVideo(courseMCQ.getVideo());
				}
				if(courseMCQ.getAudio()!=null) {
					extCourseMCQ.setAudio(courseMCQ.getAudio());
				}
				if(courseMCQ.getQuestionText()!=null) {
					extCourseMCQ.setQuestionText(courseMCQ.getQuestionText());
				}
				if(courseMCQ.getOptionAText()!=null) {
					extCourseMCQ.setOptionAText(courseMCQ.getOptionAText());
				}
				if(courseMCQ.getOptionBText()!=null) {
					extCourseMCQ.setOptionBText(courseMCQ.getOptionBText());
				}
				if(courseMCQ.getOptionCText()!=null) {
					extCourseMCQ.setOptionCText(courseMCQ.getOptionCText());
				}
				if(courseMCQ.getOptionDText()!=null) {
					extCourseMCQ.setOptionDText(courseMCQ.getOptionDText());
				}
				if(courseMCQ.getOptionEText()!=null) {
					extCourseMCQ.setOptionEText(courseMCQ.getOptionEText());
				}
				if(courseMCQ.getOptionAImage()!=null) {
					extCourseMCQ.setOptionAImage(courseMCQ.getOptionAImage());
				}
				if(courseMCQ.getOptionBImage()!=null) {
					extCourseMCQ.setOptionBImage(courseMCQ.getOptionBImage());
				}
				if(courseMCQ.getOptionCImage()!=null) {
					extCourseMCQ.setOptionCImage(courseMCQ.getOptionCImage());
				}
				if(courseMCQ.getOptionDImage()!=null) {
					extCourseMCQ.setOptionDImage(courseMCQ.getOptionDImage());
				}
				if(courseMCQ.getOptionEImage()!=null) {
					extCourseMCQ.setOptionEImage(courseMCQ.getOptionEImage());
				}
				if(courseMCQ.getCorrectAnswer()!=null) {
					extCourseMCQ.setCorrectAnswer(courseMCQ.getCorrectAnswer());
				}
				extCourseMCQ.setModifiedDate(LocalDateTime.now());
				extCourseMCQ.setModifiedBy(courseMCQ.getModifiedBy());
				courseMCQRepo.save(extCourseMCQ);
				return extCourseMCQ;
			}else {
				return null;
			}
		}else {
			return null;
		}
	}

	@Override
	public CourseMCQ getCourseMCQById(String courseMCQId) {
		return courseMCQRepo.findById(courseMCQId).orElse(null);
	}

	@Override
	public List<CourseMCQ> getCourseMCQByTournament(String tournament) {
		return courseMCQRepo.findByTournament(tournament);
	}

	@Override
	public List<CourseMCQ> getAllCourseMCQs() {
		return courseMCQRepo.findAll();
	}

}
