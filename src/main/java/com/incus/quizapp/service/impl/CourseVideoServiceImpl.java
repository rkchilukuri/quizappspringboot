/**
 * 
 */
package com.incus.quizapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incus.quizapp.model.CourseVideo;
import com.incus.quizapp.repository.CourseVideoRepository;
import com.incus.quizapp.service.CourseVideoService;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 21-Dec-2020
 * Description:
 * *******************************************************
 */
@Service
public class CourseVideoServiceImpl implements CourseVideoService {
	@Autowired
	CourseVideoRepository courseVideoRepo;
	
	@Override
	public CourseVideo addCourseVideo(CourseVideo courseVideo) {
		return courseVideoRepo.save(courseVideo);
	}


	@Override
	public CourseVideo updateCourseVideo(CourseVideo courseVideo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CourseVideo getCourseVideoById(String courseVideoId) {
		return courseVideoRepo.findById(courseVideoId).orElse(null);
	}

	@Override
	public List<CourseVideo> getCourseVideoByChapter(String chapter) {
		return courseVideoRepo.findByChapter(chapter);
	}

	@Override
	public List<CourseVideo> getAllCourseVideos() {
		return courseVideoRepo.findAll();
	}


	@Override
	public List<CourseVideo> getCourseVideoByExamCategoryCourseChapter(String examCategory, String course,
			String chapter) {
		return courseVideoRepo.findByExamCategoryAndCourseAndChapter(examCategory,course,chapter);
	}

}
