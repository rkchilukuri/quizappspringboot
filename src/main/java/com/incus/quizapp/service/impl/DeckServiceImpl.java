/**
 * 
 */
package com.incus.quizapp.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incus.quizapp.model.Deck;
import com.incus.quizapp.repository.DeckRepository;
import com.incus.quizapp.service.DeckService;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
@Service
public class DeckServiceImpl implements DeckService {

	@Autowired
	DeckRepository deckRepo;
	
	@Override
	public List<Deck> getDeckByDeckTitle(String deckTitle) {
		return deckRepo.findByDeckTitle(deckTitle);
	}

	@Override
	public Deck addDeck(Deck deck) {
		return deckRepo.save(deck);
	}

	@Override
	public Deck getDeck(String deckId) {
		return deckRepo.findById(deckId).orElse(null);
	}

	@Override
	public List<Deck> getAllDecks() {
		return deckRepo.findAll();
	}

	@Override
	public Deck updateDeck(Deck deck) {
		if(deck.getId()!=null) {
			Deck extDeck = deckRepo.findById(deck.getId()).orElse(null);
			if(extDeck!=null) {
				if(deck.getDeckTitle()!=null) {
					extDeck.setDeckTitle(deck.getDeckTitle());
				}
				if(deck.getDescription()!=null) {
					extDeck.setDescription(deck.getDescription());
				}
				if(deck.getCoverImage()!=null) {
					extDeck.setCoverImage(deck.getCoverImage());
				}
				if(deck.getPublished()!=null) {
					extDeck.setPublished(deck.getPublished());
				}
				if(deck.getPreview()!=null) {
					extDeck.setPreview(deck.getPreview());
				}
				extDeck.setModifiedDate(LocalDateTime.now());
				extDeck.setModifiedBy(deck.getModifiedBy());
				deckRepo.save(extDeck);
				return extDeck;
			}else {
				return null;
			}
		}else {
			return null;
		}
	}

	@Override
	public List<Deck> getByFlashcardCourse(String flashcardCourse) {
		return deckRepo.findByFlashcardCourse(flashcardCourse);
	}
}
