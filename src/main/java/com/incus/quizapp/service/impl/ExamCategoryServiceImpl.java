/**
 * 
 */
package com.incus.quizapp.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incus.quizapp.model.Course;
import com.incus.quizapp.model.ExamCategory;
import com.incus.quizapp.repository.ExamCategoryRepository;
import com.incus.quizapp.service.ExamCategoryService;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 19-Dec-2020
 * Description:
 * *******************************************************
 */
@Service
public class ExamCategoryServiceImpl implements ExamCategoryService {

	@Autowired
	ExamCategoryRepository categoryRepo;
	
	@Override
	public ExamCategory addExamCategory(ExamCategory examCategory) {
		return categoryRepo.save(examCategory);
	}

	@Override
	public ExamCategory updateExamCategory(ExamCategory examCategory) {
		if(examCategory.getId()!=null) {
			ExamCategory extExamCategory = categoryRepo.findById(examCategory.getId()).orElse(null);
			if(extExamCategory!=null) {
				if(examCategory.getCategoryName()!=null) {
					extExamCategory.setCategoryName(examCategory.getCategoryName());
				}
				if(examCategory.getDescription()!=null) {
					extExamCategory.setDescription(examCategory.getDescription());
				}
				if(examCategory.getCoverImage()!=null) {
					extExamCategory.setCoverImage(examCategory.getCoverImage());
				}
				if(examCategory.getCountry()!=null) {
					extExamCategory.setCountry(examCategory.getCountry());
				}
				extExamCategory.setModifiedDate(LocalDateTime.now());
				extExamCategory.setModifiedBy(examCategory.getModifiedBy());
				categoryRepo.save(extExamCategory);
				return extExamCategory;
			}else {
				return null;
			}
		}else {
			return null;
		}
	}

	@Override
	public List<ExamCategory> getAllExamCategorys() {
		return categoryRepo.findAll();
	}

	@Override
	public List<ExamCategory> getExamCategoryByCategoryName(String examCategoryName) {
		return categoryRepo.findByCategoryName(examCategoryName);
	}

	@Override
	public ExamCategory getExamCategoryById(String examCategoryId) {
		return categoryRepo.findById(examCategoryId).orElse(null);
	}

	@Override
	public List<ExamCategory> getExamCategoryByCountry(String country) {
		return categoryRepo.findByCountry(country);
	}

}
