/**
 * 
 */
package com.incus.quizapp.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incus.quizapp.model.Course;
import com.incus.quizapp.model.FlashcardCourse;
import com.incus.quizapp.repository.FlashcardCourseRepository;
import com.incus.quizapp.service.FlashcardCourseService;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
@Service
public class FlashcardCourseServiceImpl implements FlashcardCourseService {

	@Autowired
	FlashcardCourseRepository courseRepo;
	
	@Override
	public List<FlashcardCourse> getCourseByCourseTitle(String courseTitle) {
		return courseRepo.findByFlashcardTitle(courseTitle);
	}

	@Override
	public FlashcardCourse addCourse(FlashcardCourse course) {
		return courseRepo.save(course);
	}

	@Override
	public FlashcardCourse getCourse(String courseId) {
		return courseRepo.findById(courseId).orElse(null);
	}

	@Override
	public List<FlashcardCourse> getAllCourses() {
		return courseRepo.findAll();
	}

	@Override
	public FlashcardCourse updateCourse(FlashcardCourse course) {
		if(course.getId()!=null) {
			FlashcardCourse extCourse = courseRepo.findById(course.getId()).orElse(null);
			if(extCourse!=null) {
				if(course.getFlashcardTitle()!=null) {
					extCourse.setFlashcardTitle(course.getFlashcardTitle());
				}
				if(course.getAboutAuthor()!=null) {
					extCourse.setAboutAuthor(course.getAboutAuthor());
				}
				if(course.getAboutCourse()!=null) {
					extCourse.setAboutCourse(course.getAboutCourse());
				}
				if(course.getAuthorName()!=null) {
					extCourse.setAuthorName(course.getAuthorName());
				}
				if(course.getCoverImage()!=null) {
					extCourse.setCoverImage(course.getCoverImage());
				}
				if(course.getExamCategory()!=null) {
					extCourse.setExamCategory(course.getExamCategory());
				}
				if(course.getTotalNumberOfMCQsOffered()!=null) {
					extCourse.setTotalNumberOfMCQsOffered(course.getTotalNumberOfMCQsOffered());
				}
				if(course.getPriceOfTheCourse()!=null) {
					extCourse.setPriceOfTheCourse(course.getPriceOfTheCourse());
				}
				if(course.getForPublic()!=null) {
					extCourse.setForPublic(course.getForPublic());
				}
				if(course.getPublished()!=null) {
					extCourse.setPublished(course.getPublished());
				}
				if(course.getSubscriptionDuration()!=null) {
					extCourse.setSubscriptionDuration(course.getSubscriptionDuration());
				}
				if(course.getTotalNumberOfTournaments()!=null) {
					extCourse.setTotalNumberOfTournaments(course.getTotalNumberOfTournaments());
				}
				extCourse.setModifiedDate(LocalDateTime.now());
				extCourse.setModifiedBy(course.getModifiedBy());
				courseRepo.save(extCourse);
				return extCourse;
			}else {
				return null;
			}
		}else {
			return null;
		}
	}

	@Override
	public List<FlashcardCourse> getCourseByExamCategory(String examCategory) {
		return courseRepo.findByExamCategory(examCategory);
	}

	@Override
	public List<Course> getCourseByCreator(String createdBy) {
		return courseRepo.findByCreatedBy(createdBy);
	}
}
