/**
 * 
 */
package com.incus.quizapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incus.quizapp.model.Flashcard;
import com.incus.quizapp.repository.FlashcardRepository;
import com.incus.quizapp.service.FlashcardService;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 22-Dec-2020
 * Description:
 * *******************************************************
 */
@Service
public class FlashcardServiceImpl implements FlashcardService{
	@Autowired
	FlashcardRepository flashcardRepo;

	@Override
	public Flashcard addFlashcard(Flashcard flashcard) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Flashcard updateFlashcard(Flashcard flashcard) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Flashcard getFlashcard(String flashcardId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Flashcard> getFlashcardByFlashcardTitle(String flashcardTitle) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Flashcard> getAllFlashcards() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Flashcard> getFlashcardByDeck(String deck) {
		// TODO Auto-generated method stub
		return null;
	}

}
