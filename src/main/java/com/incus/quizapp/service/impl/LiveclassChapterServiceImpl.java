/**
 * 
 */
package com.incus.quizapp.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incus.quizapp.model.LiveclassChapter;
import com.incus.quizapp.repository.LiveclassChapterRepository;
import com.incus.quizapp.service.LiveclassChapterService;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 13-Dec-2020
 * Description:
 * *******************************************************
 */
@Service
public class LiveclassChapterServiceImpl implements LiveclassChapterService {

	@Autowired
	LiveclassChapterRepository chapterRepo;
	
	@Override
	public List<LiveclassChapter> getLiveclassChapterByLiveclassChapterTitle(String chapterTitle) {
		return chapterRepo.findByLiveclassChapterTitle(chapterTitle);
	}

	@Override
	public LiveclassChapter addLiveclassChapter(LiveclassChapter chapter) {
		return chapterRepo.save(chapter);
	}

	@Override
	public LiveclassChapter getLiveclassChapter(String chapterId) {
		return chapterRepo.findById(chapterId).orElse(null);
	}

	@Override
	public List<LiveclassChapter> getAllLiveclassChapters() {
		return chapterRepo.findAll();
	}

	@Override
	public LiveclassChapter updateLiveclassChapter(LiveclassChapter chapter) {
		if(chapter.getId()!=null) {
			LiveclassChapter extChapter = chapterRepo.findById(chapter.getId()).orElse(null);
			if(extChapter!=null) {
				if(chapter.getLiveclassChapterTitle()!=null) {
					extChapter.setLiveclassChapterTitle(chapter.getLiveclassChapterTitle());
				}
				if(chapter.getDescription()!=null) {
					extChapter.setDescription(chapter.getDescription());
				}
				if(chapter.getCoverImage()!=null) {
					extChapter.setCoverImage(chapter.getCoverImage());
				}
				if(chapter.getPublished()!=null) {
					extChapter.setPublished(chapter.getPublished());
				}
				if(chapter.getPreview()!=null) {
					extChapter.setPreview(chapter.getPreview());
				}
				extChapter.setModifiedDate(LocalDateTime.now());
				extChapter.setModifiedBy(chapter.getModifiedBy());
				chapterRepo.save(extChapter);
				return extChapter;
			}else {
				return null;
			}
		}else {
			return null;
		}
	}

	@Override
	public List<LiveclassChapter> getLiveclassChapterByCourse(String course) {
		return chapterRepo.findByCourse(course);
	}

}
