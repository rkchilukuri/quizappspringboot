/**
 * 
 */
package com.incus.quizapp.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incus.quizapp.model.Tournament;
import com.incus.quizapp.repository.TournamentRepository;
import com.incus.quizapp.service.TournamentService;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
@Service
public class TournamentServiceImpl implements TournamentService {

	@Autowired
	TournamentRepository tournamentRepo;
	
	@Override
	public List<Tournament> getTournamentByTournamentTitle(String tournamentTitle) {
		return tournamentRepo.findByTournamentTitle(tournamentTitle);
	}

	@Override
	public Tournament addTournament(Tournament tournament) {
		return tournamentRepo.save(tournament);
	}

	@Override
	public Tournament getTournament(String tournamentId) {
		return tournamentRepo.findById(tournamentId).orElse(null);
	}

	@Override
	public List<Tournament> getAllTournaments() {
		return tournamentRepo.findAll();
	}

	@Override
	public Tournament updateTournament(Tournament tournament) {
		if(tournament.getId()!=null) {
			Tournament extTournament = tournamentRepo.findById(tournament.getId()).orElse(null);
			if(extTournament!=null) {
				if(tournament.getExamCategory()!=null) {
					extTournament.setExamCategory(tournament.getExamCategory());
				}
				if(tournament.getCourse()!=null) {
					extTournament.setCourse(tournament.getCourse());
				}
				if(tournament.getChapter()!=null) {
					extTournament.setChapter(tournament.getChapter());
				}
				if(tournament.getTournamentTitle()!=null) {
					extTournament.setTournamentTitle(tournament.getTournamentTitle());
				}
				if(tournament.getCoverImage()!=null) {
					extTournament.setCoverImage(tournament.getCoverImage());
				}
				if(tournament.getDescription()!=null) {
					extTournament.setDescription(tournament.getDescription());
				}
				if(tournament.getPreview()!=null) {
					extTournament.setPreview(tournament.getPreview());
				}
				if(tournament.getPublished()!=null) {
					extTournament.setPublished(tournament.getPublished());
				}
				extTournament.setModifiedDate(LocalDateTime.now());
				extTournament.setModifiedBy(tournament.getModifiedBy());
				tournamentRepo.save(extTournament);
				return extTournament;
			}else {
				return null;
			}
		}else {
			return null;
		}
	}

	@Override
	public List<Tournament> getTournamentByCategoryCourseChapter(String examCategory, String course, String chapter) {
		return tournamentRepo.findByExamCategoryAndCourseAndChapter(examCategory, course, chapter);
	}

}
