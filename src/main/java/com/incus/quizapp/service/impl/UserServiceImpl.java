/**
 * 
 */
package com.incus.quizapp.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incus.quizapp.model.User;
import com.incus.quizapp.repository.UserRepository;
import com.incus.quizapp.service.UserService;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 19-Dec-2020
 * Description:
 * *******************************************************
 */
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	UserRepository userRepo;
	
	@Override
	public List<User> getUserByUserName(String userName) {
		List<User> users = userRepo.findByName(userName);
		if(users!=null && users.size()>0) {
			return users;
		}else {
			return null;
		}
	}

	@Override
	public User updateUser(User user) {
		if(user.getId()!=null) {
			User extUser = userRepo.findById(user.getId()).orElse(null);
			if(extUser!=null) {
				if(user.getUserAlias()!=null) {
					extUser.setUserAlias(user.getUserAlias());
				}
				if(user.getCity()!=null) {
					extUser.setCity(user.getCity());
				}
				if(user.getPhoneNo()!=null) {
					extUser.setPhoneNo(user.getPhoneNo());
				}
				if(user.getDob()!=null) {
					extUser.setDob(user.getDob());
				}
				if(user.getEmailId()!=null) {
					extUser.setEmailId(user.getEmailId());
				}
				if(user.getRegisterFrom()!=null) {
					extUser.setRegisterFrom(user.getRegisterFrom());
				}
				if(user.getProfilePic()!=null) {
					extUser.setProfilePic(user.getProfilePic());
				}
				if(user.getRole()!=null) {
					extUser.setRole(user.getRole());
				}
				if(user.getDob()!=null) {
					extUser.setDob(user.getDob());
				}
				if(user.getDob()!=null) {
					extUser.setDob(user.getDob());
				}
				extUser.setModifiedDate(LocalDateTime.now());
				extUser.setModifiedBy(user.getModifiedBy());
				userRepo.save(extUser);
				return extUser;
			}else {
				return null;
			}
		}else {
			return null;
		}
	}

	@Override
	public User addUser(User user) {
		return userRepo.save(user);
	}

	@Override
	public List<User> getAllUsers() {
		return userRepo.findAll();
	}

	@Override
	public User getUser(String userId) {
		return userRepo.findById(userId).orElse(null);
	}

	@Override
	public User getUserByPhoneNo(String phoneNo) {
		List<User> users = userRepo.findByPhoneNo(phoneNo);
		if(users!=null && users.size()>0) {
			return users.get(0);
		}else {
			return null;
		}
	}
	
	@Override
	public User getUserByEmail(String email) {
		List<User> users = userRepo.findByEmailId(email);
		if(users!=null && users.size()>0) {
			return users.get(0);
		}else {
			return null;
		}
	}

	@Override
	public List<User> getUserByUserNameAndPassword(String userName, String password) {
		return userRepo.findByNameAndPassword(userName,password);
	}
}
