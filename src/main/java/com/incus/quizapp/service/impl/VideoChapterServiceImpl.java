/**
 * 
 */
package com.incus.quizapp.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incus.quizapp.model.Chapter;
import com.incus.quizapp.model.VideoChapter;
import com.incus.quizapp.repository.VideoChapterRepository;
import com.incus.quizapp.service.VideoChapterService;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
@Service
public class VideoChapterServiceImpl implements VideoChapterService {

	@Autowired
	VideoChapterRepository chapterRepo;
	
	@Override
	public List<VideoChapter> getChapterByChapterTitle(String chapterTitle) {
		return chapterRepo.findByChapterTitle(chapterTitle);
	}

	@Override
	public VideoChapter addChapter(VideoChapter chapter) {
		return chapterRepo.save(chapter);
	}

	@Override
	public VideoChapter getChapter(String chapterId) {
		return chapterRepo.findById(chapterId).orElse(null);
	}

	@Override
	public List<VideoChapter> getAllChapters() {
		return chapterRepo.findAll();
	}

	@Override
	public VideoChapter updateChapter(VideoChapter chapter) {
		if(chapter.getId()!=null) {
			VideoChapter extChapter = chapterRepo.findById(chapter.getId()).orElse(null);
			if(extChapter!=null) {
				if(chapter.getCourse()!=null) {
					extChapter.setCourse(chapter.getCourse());
				}
				if(chapter.getChapterTitle()!=null) {
					extChapter.setChapterTitle(chapter.getChapterTitle());
				}
				if(chapter.getDescription()!=null) {
					extChapter.setDescription(chapter.getDescription());
				}
				if(chapter.getCoverImage()!=null) {
					extChapter.setCoverImage(chapter.getCoverImage());
				}
				if(chapter.getPublished()!=null) {
					extChapter.setPublished(chapter.getPublished());
				}
				if(chapter.getPreview()!=null) {
					extChapter.setPreview(chapter.getPreview());
				}
				extChapter.setModifiedDate(LocalDateTime.now());
				extChapter.setModifiedBy(chapter.getModifiedBy());
				chapterRepo.save(extChapter);
				return extChapter;
			}else {
				return null;
			}
		}else {
			return null;
		}
	}

	@Override
	public List<VideoChapter> getChapterByCourse(String course) {
		return chapterRepo.findByCourse(course);
	}

}
