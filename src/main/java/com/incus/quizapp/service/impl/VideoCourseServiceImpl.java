/**
 * 
 */
package com.incus.quizapp.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.incus.quizapp.model.Course;
import com.incus.quizapp.model.VideoCourse;
import com.incus.quizapp.repository.VideoCourseRepository;
import com.incus.quizapp.service.VideoCourseService;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 12-Dec-2020
 * Description:
 * *******************************************************
 */
@Service
public class VideoCourseServiceImpl implements VideoCourseService {

	@Autowired
	VideoCourseRepository courseRepo;
	
	@Override
	public List<VideoCourse> getCourseByCourseTitle(String courseTitle) {
		return courseRepo.findByVideoTitle(courseTitle);
	}

	@Override
	public VideoCourse addCourse(VideoCourse course) {
		return courseRepo.save(course);
	}

	@Override
	public VideoCourse getCourse(String courseId) {
		return courseRepo.findById(courseId).orElse(null);
	}

	@Override
	public List<VideoCourse> getAllCourses() {
		return courseRepo.findAll();
	}

	@Override
	public VideoCourse updateCourse(VideoCourse course) {
		if(course.getId()!=null) {
			VideoCourse extCourse = courseRepo.findById(course.getId()).orElse(null);
			if(extCourse!=null) {
				if(course.getVideoTitle()!=null) {
					extCourse.setVideoTitle(course.getVideoTitle());
				}
				if(course.getAboutAuthor()!=null) {
					extCourse.setAboutAuthor(course.getAboutAuthor());
				}
				if(course.getAboutCourse()!=null) {
					extCourse.setAboutCourse(course.getAboutCourse());
				}
				if(course.getAuthorName()!=null) {
					extCourse.setAuthorName(course.getAuthorName());
				}
				if(course.getCoverImage()!=null) {
					extCourse.setCoverImage(course.getCoverImage());
				}
				if(course.getExamCategory()!=null) {
					extCourse.setExamCategory(course.getExamCategory());
				}
				if(course.getTotalNumberOfMCQsOffered()!=null) {
					extCourse.setTotalNumberOfMCQsOffered(course.getTotalNumberOfMCQsOffered());
				}
				if(course.getPriceOfTheCourse()!=null) {
					extCourse.setPriceOfTheCourse(course.getPriceOfTheCourse());
				}
				if(course.getForPublic()!=null) {
					extCourse.setForPublic(course.getForPublic());
				}
				if(course.getPublished()!=null) {
					extCourse.setPublished(course.getPublished());
				}
				if(course.getSubscriptionDuration()!=null) {
					extCourse.setSubscriptionDuration(course.getSubscriptionDuration());
				}
				if(course.getTotalNumberOfTournaments()!=null) {
					extCourse.setTotalNumberOfTournaments(course.getTotalNumberOfTournaments());
				}
				extCourse.setModifiedDate(LocalDateTime.now());
				extCourse.setModifiedBy(course.getModifiedBy());
				courseRepo.save(extCourse);
				return extCourse;
			}else {
				return null;
			}
		}else {
			return null;
		}
	}

	@Override
	public List<VideoCourse> getCourseByExamCategory(String examCategory) {
		return courseRepo.findByExamCategory(examCategory);
	}

	@Override
	public List<Course> getCourseByCreator(String createdBy) {
		return courseRepo.findByCreatedBy(createdBy);
	}

}
