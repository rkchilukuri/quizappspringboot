/**
 * 
 */
package com.incus.quizapp.util;

/**
 * *********************INCUS*****************************
 * Copyright (c) 2020-2025 Incus Ltd. All Rights Reserved.
 * Author ramakrishna
 * CreatedDate: 29-Aug-2020
 * Description:
 * *******************************************************
 */
public class Constants {

	public static final String FIELD_EMAIL = "EMAIL";
	public static final String FIELD_MOBILE = "MOBILE";
	public static final String ERR_USR_001 = "Error in Adding User";
	public static final String ERR_USR_002 = "User does not exist";
	public static final String ERR_USR_003 = "Error updating User Preferences";
	public static final String ERR_USR_004 = "User Alreay Exist";
	public static final String ERR_USR_005 = "User Email Id is Mandatory";
	public static final String ERR_USR_006 = "Error in adding User Payment";
	public static final String ERR_USR_007 = "Alias Name already in use";
	public static final String ERR_CPT_001 = "Error in Adding Chapter";
	public static final String ERR_CPT_002 = "Chapter does not exist";
	public static final String ERR_CPT_003 = "Chapter Already exist";
	public static final String ERR_CPT_004 = "Chapter Name is Mandatory";
	public static final String ERR_CRS_001 = "Error in Adding Course";
	public static final String ERR_CRS_002 = "Course does not exist";
	public static final String ERR_CRS_003 = "Course Already exist";
	public static final String ERR_CRS_004 = "Course Name is Mandatory";
	public static final String ERR_VCRS_001 = "Error in Adding Video Course";
	public static final String ERR_VCRS_002 = "Video Course does not exist";
	public static final String ERR_VCRS_003 = "Video Course Already exist";
	public static final String ERR_VCRS_004 = "Video Course Name is Mandatory";
	public static final String ERR_FCRS_001 = "Error in Adding Flashcard Course";
	public static final String ERR_FCRS_002 = "Flashcard Course does not exist";
	public static final String ERR_FCRS_003 = "Flashcard Course Already exist";
	public static final String ERR_FCRS_004 = "Flashcard Course Name is Mandatory";
	public static final String ERR_LCRS_001 = "Error in Adding Liveclass Course";
	public static final String ERR_LCRS_002 = "Liveclass Course does not exist";
	public static final String ERR_LCRS_003 = "Liveclass Course Already exist";
	public static final String ERR_LCRS_004 = "Liveclass Course Name is Mandatory";
	public static final String ERR_USRN_001 = "Notifications Doesnot Exist for this User";
	public static final String ERR_CHT_001 = "Chat History Not Found";
	public static final String ERR_CHT_002 = "Error in Adding ChatMessage";
	public static final String ERR_VID_001 = "Error in Adding Video";
	public static final String ERR_VID_002 = "Video does not exist";
	public static final String ERR_VID_003 = "Error in Updating Video";
	public static final String ERR_VID_004 = "Video Already exist";
	public static final String ERR_VIDC_001 = "Error in Adding Video comment";
	public static final String ERR_VIDC_002 = "Video comments not found";
}
